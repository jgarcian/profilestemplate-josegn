json.extract! permission, :id, :name, :crear, :editar, :leer, :eliminar, :permission_id, :created_at, :updated_at
json.url permission_url(permission, format: :json)
