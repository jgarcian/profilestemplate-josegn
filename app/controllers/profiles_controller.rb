class ProfilesController < ApplicationController
  before_action :set_profile, only: [:show, :edit, :update, :destroy]

  @@name_profiles = "Profiles"
  @@byPerfiles = "Profiles"
  # GET /profiles
  # GET /profiles.json
  def index
    if current_user
      #HISTORIC
      @page = "Profiles"
      @user = current_user.name
      @user_id = current_user.id
      @userl = current_user.last_name
      @action = "3"
      @account = "not yet"
      @lasac = "not available"
      @cli_id = 0
      @date = Time.now
      @description = Profile.all.count.to_i

      # @historic = Historic.new
      # @historic.page = @page
      # @historic.user_name = @user + " " + @userl
      # @historic.user_id = @user_id
      # @historic.action = @action
      # @historic.account = @account
      # @historic.last_account = @lasac
      # @historic.client_id = @cli_id
      # @historic.date = @date
      # @historic.description = @description
      # @historic.save
      # END HISTORIC

      pd = Profile.all
      a = Area.all

      gon.area = a
      gon.pflag = pd
      gon.false = true

      @cu = current_user.profile_id

      if @cu != 0

        @per = Permission.where("view_name = 'Profiles' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearPerfil = permisos.crear
          @editarPerfil = permisos.editar
          @leerPerfil = permisos.leer
          @eliminarPerfil = permisos.eliminar

          if permisos.view_name == @@byPerfiles

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar
          end

        end

        if ((@@crear == 8) || (@@editar == 4) || (@@leer == 2) || (@@eliminar == 1))


          # @cu_area = current_user.area_id

          # SI ES UN ÁREA CENTRAL MUESTRA TODA LA INFORMACIÓN
          # if @cu_area == "1"
          #   @profiles = Profile.where("(flag != 2) and (area_id != 1 or flag != 1)").page(params[:page]).per(10)
          # else #CUALQUIER OTRA ÁREA, MUESTRA SÓLO LA INFORMACIÓN DEL ÁREA.
          #   @profiles = Profile.where("area_id = ? and flag != 2", @cu_area).page(params[:page]).per(10)
          # end

          @area_usuario = current_user.area_id

          if @area_usuario == "1"

            areas_arr = Array.new
            perfiles_arr = Array.new

            @areas = Area.all

            @areas.each do |ar|
              valor = {id: '', name: '', area_id: ''}
              valor[:id] = ar.id
              valor[:name] = ar.name
              valor[:area_id] = ar.area_id

              areas_arr.push(valor)

              if ar.id == 1
                @perfiles = Profile.where("(area_id = ?) and (flag != 2) and (area_id != 1 or flag != 1)", ar.id)
              else #CUALQUIER OTRA ÁREA, MUESTRA SÓLO LA INFORMACIÓN DEL ÁREA.
                @perfiles = Profile.where("(area_id = ?) and (flag != 2)", ar.id)
              end

              @perfiles.each do |per|
                registro = {id: '', name: '', flag: '', area_id: ''}
                registro[:id] = per.id
                registro[:name] = per.name
                registro[:flag] = per.flag
                registro[:area_id] = per.area_id

                perfiles_arr.push(registro)
              end
            end

            @areas = areas_arr
            @profiles = Kaminari.paginate_array(perfiles_arr).page(params[:page]).per(10)
          else
            @areas = Area.where("id = ?", @area_usuario)

            areas_arr = Array.new
            perfiles_arr = Array.new

            @areas.each do |ar|
              @nombre = ar.name
              valor = {id: '', name: '', area_id: ''}
              valor[:id] = ar.id
              valor[:name] = ar.name
              valor[:area_id] = ar.area_id

              areas_arr.push(valor)

              if ar.id == 1
                @perfiles = Profile.where("(area_id = ?) and (flag != 2) and (area_id != 1 or flag != 1)", ar.id)
              else #CUALQUIER OTRA ÁREA, MUESTRA SÓLO LA INFORMACIÓN DEL ÁREA.
                @perfiles = Profile.where("(area_id = ?) and (flag != 2)", ar.id)
              end

              @perfiles.each do |per|
                registro = {id: '', name: '', flag: '', area_id: ''}
                registro[:id] = per.id
                registro[:name] = per.name
                registro[:flag] = per.flag
                registro[:area_id] = per.area_id

                perfiles_arr.push(registro)
              end

              @subarea_1 = Area.where("area_id = ?", ar.id)

              if !@subarea_1.nil?

                @subarea_1.each do |sub|
                  valor = {id: '', name: '', area_id: ''}
                  valor[:id] = sub.id
                  valor[:name] = sub.name
                  valor[:area_id] = sub.area_id

                  areas_arr.push(valor)

                  @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub.id)

                  @perfiles.each do |per|
                    registro = {id: '', name: '', flag: '', area_id: ''}
                    registro[:id] = per.id
                    registro[:name] = per.name
                    registro[:flag] = per.flag
                    registro[:area_id] = per.area_id

                    perfiles_arr.push(registro)
                  end

                  @subarea_2 = Area.where("area_id = ?", sub.id)

                  if !@subarea_2.nil?

                    @subarea_2.each do |sub2|
                      valor = {id: '', name: '', area_id: ''}
                      valor[:id] = sub2.id
                      valor[:name] = sub2.name
                      valor[:area_id] = sub2.area_id

                      areas_arr.push(valor)

                      @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub2.id)

                      @perfiles.each do |per|
                        registro = {id: '', name: '', flag: '', area_id: ''}
                        registro[:id] = per.id
                        registro[:name] = per.name
                        registro[:flag] = per.flag
                        registro[:area_id] = per.area_id

                        perfiles_arr.push(registro)
                      end

                      @subarea_3 = Area.where("area_id = ?", sub2.id)

                      if !@subarea_3.nil?

                        @subarea_3.each do |sub3|
                          valor = {id: '', name: '', area_id: ''}
                          valor[:id] = sub3.id
                          valor[:name] = sub3.name
                          valor[:area_id] = sub3.area_id

                          areas_arr.push(valor)

                          @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub3.id)

                          @perfiles.each do |per|
                            registro = {id: '', name: '', flag: '', area_id: ''}
                            registro[:id] = per.id
                            registro[:name] = per.name
                            registro[:flag] = per.flag
                            registro[:area_id] = per.area_id

                            perfiles_arr.push(registro)
                          end

                          @subarea_4 = Area.where("area_id = ?", sub3.id)

                          if !@subarea_4.nil?

                            @subarea_4.each do |sub4|
                              valor = {id: '', name: '', area_id: ''}
                              valor[:id] = sub4.id
                              valor[:name] = sub4.name
                              valor[:area_id] = sub4.area_id

                              areas_arr.push(valor)

                              @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub4.id)

                              @perfiles.each do |per|
                                registro = {id: '', name: '', flag: '', area_id: ''}
                                registro[:id] = per.id
                                registro[:name] = per.name
                                registro[:flag] = per.flag
                                registro[:area_id] = per.area_id

                                perfiles_arr.push(registro)
                              end

                              @subarea_5 = Area.where("area_id = ?", sub4.id)

                              if !@subarea_5.nil?

                                @subarea_5.each do |sub5|
                                  valor = {id: '', name: '', area_id: ''}
                                  valor[:id] = sub5.id
                                  valor[:name] = sub5.name
                                  valor[:area_id] = sub5.area_id

                                  areas_arr.push(valor)

                                  @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub5.id)

                                  @perfiles.each do |per|
                                    registro = {id: '', name: '', flag: '', area_id: ''}
                                    registro[:id] = per.id
                                    registro[:name] = per.name
                                    registro[:flag] = per.flag
                                    registro[:area_id] = per.area_id

                                    perfiles_arr.push(registro)
                                  end

                                  @subarea_6 = Area.where("area_id = ?", sub5.id)

                                  if !@subarea_6.nil?

                                    @subarea_6.each do |sub6|
                                      valor = {id: '', name: '', area_id: ''}
                                      valor[:id] = sub6.id
                                      valor[:name] = sub6.name
                                      valor[:area_id] = sub6.area_id

                                      areas_arr.push(valor)

                                      @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub6.id)

                                      @perfiles.each do |per|
                                        registro = {id: '', name: '', flag: '', area_id: ''}
                                        registro[:id] = per.id
                                        registro[:name] = per.name
                                        registro[:flag] = per.flag
                                        registro[:area_id] = per.area_id

                                        perfiles_arr.push(registro)
                                      end

                                      @subarea_7 = Area.where("area_id = ?", sub6.id)

                                      if !@subarea_7.nil?

                                        @subarea_7.each do |sub7|
                                          valor = {id: '', name: '', area_id: ''}
                                          valor[:id] = sub7.id
                                          valor[:name] = sub7.name
                                          valor[:area_id] = sub7.area_id

                                          areas_arr.push(valor)

                                          @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub7.id)

                                          @perfiles.each do |per|
                                            registro = {id: '', name: '', flag: '', area_id: ''}
                                            registro[:id] = per.id
                                            registro[:name] = per.name
                                            registro[:flag] = per.flag
                                            registro[:area_id] = per.area_id

                                            perfiles_arr.push(registro)
                                          end

                                          @subarea_8 = Area.where("area_id = ?", sub7.id)

                                          if !@subarea_8.nil?

                                            @subarea_8.each do |sub8|
                                              valor = {id: '', name: '', area_id: ''}
                                              valor[:id] = sub8.id
                                              valor[:name] = sub8.name
                                              valor[:area_id] = sub8.area_id

                                              areas_arr.push(valor)

                                              @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub8.id)

                                              @perfiles.each do |per|
                                                registro = {id: '', name: '', flag: '', area_id: ''}
                                                registro[:id] = per.id
                                                registro[:name] = per.name
                                                registro[:flag] = per.flag
                                                registro[:area_id] = per.area_id

                                                perfiles_arr.push(registro)
                                              end

                                              @subarea_9 = Area.where("area_id = ?", sub8.id)

                                              if !@subarea_9.nil?

                                                @subarea_9.each do |sub9|
                                                  valor = {id: '', name: '', area_id: ''}
                                                  valor[:id] = sub9.id
                                                  valor[:name] = sub9.name
                                                  valor[:area_id] = sub9.area_id

                                                  areas_arr.push(valor)

                                                  @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub9.id)

                                                  @perfiles.each do |per|
                                                    registro = {id: '', name: '', flag: '', area_id: ''}
                                                    registro[:id] = per.id
                                                    registro[:name] = per.name
                                                    registro[:flag] = per.flag
                                                    registro[:area_id] = per.area_id

                                                    perfiles_arr.push(registro)
                                                  end

                                                  @subarea_10 = Area.where("area_id = ?", sub9.id)

                                                  if !@subarea_10.nil?

                                                    @subarea_10.each do |sub10|
                                                      valor = {id: '', name: '', area_id: ''}
                                                      valor[:id] = sub10.id
                                                      valor[:name] = sub10.name
                                                      valor[:area_id] = sub10.area_id

                                                      areas_arr.push(valor)

                                                      @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub10.id)

                                                      @perfiles.each do |per|
                                                        registro = {id: '', name: '', flag: '', area_id: ''}
                                                        registro[:id] = per.id
                                                        registro[:name] = per.name
                                                        registro[:flag] = per.flag
                                                        registro[:area_id] = per.area_id

                                                        perfiles_arr.push(registro)
                                                      end
                                                    end
                                                  end
                                                end
                                              end
                                            end
                                          end
                                        end
                                      end
                                    end
                                  end
                                end
                              end
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end
            end

            @areas = areas_arr
            @profiles = Kaminari.paginate_array(perfiles_arr).page(params[:page]).per(10)
          end


          if params[:search].present?
            @belongs = params[:search].to_s

            if @belongs == "Central Office" || @belongs == "Oficina Central" || @belongs == "Central Office\t" || @belongs == "Oficina Central\t" || @belongs == "Central Office\t\t" || @belongs == "Oficina Central\t\t" || @belongs == "central office" || @belongs == "oficina central" || @belongs == "central office\t" || @belongs == "oficina central\t" || @belongs == "central office\t\t" || @belongs == "oficina central\t\t"
              @belongs= nil
              @busco = "2"
            elsif @belongs == "Store" || @belongs == "Tienda" || @belongs == "Store\t" || @belongs == "Tienda\t" || @belongs == "Store\t\t" || @belongs == "Tienda\t\t" || @belongs == "store" || @belongs == "tienda" || @belongs == "store\t" || @belongs == "tienda\t" || @belongs == "store\t\t" || @belongs == "tienda\t\t"
              @belongs = "1"
              @busco = "1"
            end

            @area = params[:search].to_s
            @areas = Area.find_by_name(@area)
            if @areas.present?
              @area_id = @areas.id
            end

            name = params[:search]
            name = name.gsub! /\t/, ''
            if name.nil?
              @na = params[:search].to_s
            else
              @na = name
            end

            if !Profile.find_by_name(@na).nil?
              if @cu_area == "1"
                @profiles = Profile.where("name = ? and (flag != 2) and (area_id != 1 or flag != 1)", @na).page(params[:page]).per(10)
                @query = params[:search]
              else
                @profiles = Profile.where("name = ? and area_id = ? and flag != 2", @na, @cu_area).page(params[:page]).per(10)
                @query = params[:search]
              end

              if @profiles.nil? || !@profiles.present? || @profiles == ""
                @error = t('all.error')
                @query = params[:search]
                if @cu_area == "1"
                  @profiles = Profile.where("(flag != 2) and (area_id != 1 or flag != 1)").page(params[:page]).per(10)
                else
                  @profiles = Profile.where("area_id = ? and flag != 2", @cu_area).page(params[:page]).per(10)
                end
              end
            elsif !Profile.find_by_area_id(@area_id).nil?
              if @cu_area == "1"
                @profiles = Profile.where("area_id = ? and (flag != 2) and (area_id != 1 or flag != 1)", @area_id).page(params[:page]).per(10)
                @query = params[:search]
              else
                @profiles = Profile.where("area_id = ? and area_id = ? and flag != 2", @area_id, @cu_area).page(params[:page]).per(10)
                @query = params[:search]
              end

              if @profiles.nil? || !@profiles.present? || @profiles == ""
                @error = t('all.error')
                @query = params[:search]
                if @cu_area == "1"
                  @profiles = Profile.where("(flag != 2) and (area_id != 1 or flag != 1)").page(params[:page]).per(10)
                else
                  @profiles = Profile.where("area_id = ? and flag != 2", @cu_area).page(params[:page]).per(10)
                end
              end
            elsif !Profile.find_by_store_id(@belongs).nil?
              if @cu_area == "1"
                if @busco == "2"
                  @profiles = Profile.where("store_id IS NULL and (flag != 2) and (area_id != 1 or flag != 1)").page(params[:page]).per(10)
                  @query = params[:search]
                elsif @busco == "1"
                  @profiles = Profile.where("store_id = ? and (flag != 2) and (area_id != 1 or flag != 1)", @busco).page(params[:page]).per(10)
                  @query = params[:search]
                else
                  @error = t('all.error')
                  @query = params[:search]
                  @profiles = Profile.where("(flag != 2) and (area_id != 1 or flag != 1)").page(params[:page]).per(10)
                end
              else
                if @busco == "2"
                  @profiles = Profile.where("store_id IS NULL and area_id = ? and flag != 2", @cu_area).page(params[:page]).per(10)
                  @query = params[:search]
                elsif @busco == "1"
                  @profiles = Profile.where("store_id = ? and area_id = ? and flag != 2", @busco, @cu_area).page(params[:page]).per(10)
                  @query = params[:search]
                else
                  @error = t('all.error')
                  @query = params[:search]
                  @profiles = Profile.where("area_id = ? and flag != 2", @cu_area).page(params[:page]).per(10)
                end

                if @profiles.nil? || !@profiles.present? || @profiles == ""
                  @error = t('all.error')
                  @query = params[:search]
                  if @cu_area == "1"
                    @profiles = Profile.where("(flag != 2) and (area_id != 1 or flag != 1)").page(params[:page]).per(10)
                  else
                    @profiles = Profile.where("area_id = ? and flag != 2", @cu_area).page(params[:page]).per(10)
                  end
                end
              end
            else
              @error = t('all.error')
              @query = params[:search]
              if @cu_area =="1"
                @profiles = Profile.where("(flag != 2) and (area_id != 1 or flag != 1)").page(params[:page]).per(10)
              else
                @profiles = Profile.where("area_id = ? and flag != 2", @cu_area).page(params[:page]).per(10)
              end
            end
          end

        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # GET /profiles/1
  # GET /profiles/1.json
  def show
    if current_user
      @page = "Profiles"
      @user = current_user.name
      @user_id = current_user.id
      @userl = current_user.last_name
      @action = "5"
      @account = "not yet"
      @lasac = "not available"
      @cli_id = 0
      #@det = @loyalty_client.params[:id]
      @date = Time.now

      # @historic = Historic.new
      # @historic.page = @page
      # @historic.user_name = @user + " " + @userl
      # @historic.user_id = @user_id
      # #@historic.action = @det
      # @historic.action = @action
      # @historic.account = @account
      # @historic.last_account = @lasac
      # @historic.client_id = @cli_id
      # @historic.date = @date
      # @historic.save

      @cu = current_user.profile_id
      pd = Profile.all
      a = Area.all

      gon.area = a
      gon.pflag = pd
      gon.false = true

      if @cu != 0
        @per = Permission.where("view_name = 'Profiles' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarPerfil = permisos.editar

          if permisos.view_name == @@byPerfiles

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@leer == 2) && ((@profile.flag != 2) && (@profile.flag != 1 || @profile.area_id != 1)))

          # if current_user.area_id == "1"
          #   @profile_id = params[:id]
          #   @total_profile = Permission.where("profile_id = ?", @profile_id)
          # else
          #   if @profile.area_id == current_user.area_id.to_i
          #     @profile_id = params[:id]
          #     @total_profile = Permission.where("profile_id = ?", @profile_id)
          #   else
          #     @Without_Permission = 100
          #     redirect_to home_index_path, :alert => t('all.not_access')
          #   end
          # end


          @area_usuario = current_user.area_id

          if @area_usuario == "1"

            @profile_id = params[:id]
            @total_profile = Permission.where("profile_id = ?", @profile_id)

          else
            @areas = Area.where("id = ?", @area_usuario)

            areas_arr = Array.new
            perfiles_arr = Array.new

            @areas.each do |ar|
              @nombre = ar.name
              valor = {id: '', name: '', area_id: ''}
              valor[:id] = ar.id
              valor[:name] = ar.name
              valor[:area_id] = ar.area_id

              areas_arr.push(valor)

              if ar.id == 1
                @perfiles = Profile.where("(area_id = ?) and (flag != 2) and (area_id != 1 or flag != 1)", ar.id)
              else #CUALQUIER OTRA ÁREA, MUESTRA SÓLO LA INFORMACIÓN DEL ÁREA.
                @perfiles = Profile.where("(area_id = ?) and (flag != 2)", ar.id)
              end

              @perfiles.each do |per|
                registro = {id: '', name: '', flag: '', area_id: ''}
                registro[:id] = per.id
                registro[:name] = per.name
                registro[:flag] = per.flag
                registro[:area_id] = per.area_id

                perfiles_arr.push(registro)
              end

              @subarea_1 = Area.where("area_id = ?", ar.id)

              if !@subarea_1.nil?

                @subarea_1.each do |sub|
                  valor = {id: '', name: '', area_id: ''}
                  valor[:id] = sub.id
                  valor[:name] = sub.name
                  valor[:area_id] = sub.area_id

                  areas_arr.push(valor)

                  @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub.id)

                  @perfiles.each do |per|
                    registro = {id: '', name: '', flag: '', area_id: ''}
                    registro[:id] = per.id
                    registro[:name] = per.name
                    registro[:flag] = per.flag
                    registro[:area_id] = per.area_id

                    perfiles_arr.push(registro)
                  end

                  @subarea_2 = Area.where("area_id = ?", sub.id)

                  if !@subarea_2.nil?

                    @subarea_2.each do |sub2|
                      valor = {id: '', name: '', area_id: ''}
                      valor[:id] = sub2.id
                      valor[:name] = sub2.name
                      valor[:area_id] = sub2.area_id

                      areas_arr.push(valor)

                      @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub2.id)

                      @perfiles.each do |per|
                        registro = {id: '', name: '', flag: '', area_id: ''}
                        registro[:id] = per.id
                        registro[:name] = per.name
                        registro[:flag] = per.flag
                        registro[:area_id] = per.area_id

                        perfiles_arr.push(registro)
                      end

                      @subarea_3 = Area.where("area_id = ?", sub2.id)

                      if !@subarea_3.nil?

                        @subarea_3.each do |sub3|
                          valor = {id: '', name: '', area_id: ''}
                          valor[:id] = sub3.id
                          valor[:name] = sub3.name
                          valor[:area_id] = sub3.area_id

                          areas_arr.push(valor)

                          @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub3.id)

                          @perfiles.each do |per|
                            registro = {id: '', name: '', flag: '', area_id: ''}
                            registro[:id] = per.id
                            registro[:name] = per.name
                            registro[:flag] = per.flag
                            registro[:area_id] = per.area_id

                            perfiles_arr.push(registro)
                          end

                          @subarea_4 = Area.where("area_id = ?", sub3.id)

                          if !@subarea_4.nil?

                            @subarea_4.each do |sub4|
                              valor = {id: '', name: '', area_id: ''}
                              valor[:id] = sub4.id
                              valor[:name] = sub4.name
                              valor[:area_id] = sub4.area_id

                              areas_arr.push(valor)

                              @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub4.id)

                              @perfiles.each do |per|
                                registro = {id: '', name: '', flag: '', area_id: ''}
                                registro[:id] = per.id
                                registro[:name] = per.name
                                registro[:flag] = per.flag
                                registro[:area_id] = per.area_id

                                perfiles_arr.push(registro)
                              end

                              @subarea_5 = Area.where("area_id = ?", sub4.id)

                              if !@subarea_5.nil?

                                @subarea_5.each do |sub5|
                                  valor = {id: '', name: '', area_id: ''}
                                  valor[:id] = sub5.id
                                  valor[:name] = sub5.name
                                  valor[:area_id] = sub5.area_id

                                  areas_arr.push(valor)

                                  @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub5.id)

                                  @perfiles.each do |per|
                                    registro = {id: '', name: '', flag: '', area_id: ''}
                                    registro[:id] = per.id
                                    registro[:name] = per.name
                                    registro[:flag] = per.flag
                                    registro[:area_id] = per.area_id

                                    perfiles_arr.push(registro)
                                  end

                                  @subarea_6 = Area.where("area_id = ?", sub5.id)

                                  if !@subarea_6.nil?

                                    @subarea_6.each do |sub6|
                                      valor = {id: '', name: '', area_id: ''}
                                      valor[:id] = sub6.id
                                      valor[:name] = sub6.name
                                      valor[:area_id] = sub6.area_id

                                      areas_arr.push(valor)

                                      @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub6.id)

                                      @perfiles.each do |per|
                                        registro = {id: '', name: '', flag: '', area_id: ''}
                                        registro[:id] = per.id
                                        registro[:name] = per.name
                                        registro[:flag] = per.flag
                                        registro[:area_id] = per.area_id

                                        perfiles_arr.push(registro)
                                      end

                                      @subarea_7 = Area.where("area_id = ?", sub6.id)

                                      if !@subarea_7.nil?

                                        @subarea_7.each do |sub7|
                                          valor = {id: '', name: '', area_id: ''}
                                          valor[:id] = sub7.id
                                          valor[:name] = sub7.name
                                          valor[:area_id] = sub7.area_id

                                          areas_arr.push(valor)

                                          @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub7.id)

                                          @perfiles.each do |per|
                                            registro = {id: '', name: '', flag: '', area_id: ''}
                                            registro[:id] = per.id
                                            registro[:name] = per.name
                                            registro[:flag] = per.flag
                                            registro[:area_id] = per.area_id

                                            perfiles_arr.push(registro)
                                          end

                                          @subarea_8 = Area.where("area_id = ?", sub7.id)

                                          if !@subarea_8.nil?

                                            @subarea_8.each do |sub8|
                                              valor = {id: '', name: '', area_id: ''}
                                              valor[:id] = sub8.id
                                              valor[:name] = sub8.name
                                              valor[:area_id] = sub8.area_id

                                              areas_arr.push(valor)

                                              @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub8.id)

                                              @perfiles.each do |per|
                                                registro = {id: '', name: '', flag: '', area_id: ''}
                                                registro[:id] = per.id
                                                registro[:name] = per.name
                                                registro[:flag] = per.flag
                                                registro[:area_id] = per.area_id

                                                perfiles_arr.push(registro)
                                              end

                                              @subarea_9 = Area.where("area_id = ?", sub8.id)

                                              if !@subarea_9.nil?

                                                @subarea_9.each do |sub9|
                                                  valor = {id: '', name: '', area_id: ''}
                                                  valor[:id] = sub9.id
                                                  valor[:name] = sub9.name
                                                  valor[:area_id] = sub9.area_id

                                                  areas_arr.push(valor)

                                                  @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub9.id)

                                                  @perfiles.each do |per|
                                                    registro = {id: '', name: '', flag: '', area_id: ''}
                                                    registro[:id] = per.id
                                                    registro[:name] = per.name
                                                    registro[:flag] = per.flag
                                                    registro[:area_id] = per.area_id

                                                    perfiles_arr.push(registro)
                                                  end

                                                  @subarea_10 = Area.where("area_id = ?", sub9.id)

                                                  if !@subarea_10.nil?

                                                    @subarea_10.each do |sub10|
                                                      valor = {id: '', name: '', area_id: ''}
                                                      valor[:id] = sub10.id
                                                      valor[:name] = sub10.name
                                                      valor[:area_id] = sub10.area_id

                                                      areas_arr.push(valor)

                                                      @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub10.id)

                                                      @perfiles.each do |per|
                                                        registro = {id: '', name: '', flag: '', area_id: ''}
                                                        registro[:id] = per.id
                                                        registro[:name] = per.name
                                                        registro[:flag] = per.flag
                                                        registro[:area_id] = per.area_id

                                                        perfiles_arr.push(registro)
                                                      end
                                                    end
                                                  end
                                                end
                                              end
                                            end
                                          end
                                        end
                                      end
                                    end
                                  end
                                end
                              end
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end
            end

            @profile_id = params[:id]
            @visible = false

            perfiles_arr.each do |per_visible|
              @id_actual = per_visible[:id]
              if @id_actual == @profile_id.to_i
                @visible = true
                break
              else
                @visible = false
              end
            end

            if @visible == true
              @total_profile = Permission.where("profile_id = ?", @profile_id)
            else
              @Without_Permission = 100
              redirect_to home_index_path, :alert => t('all.not_access')
            end

          end


        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # GET /profiles/new
  def new
    if current_user
      pd = Profile.all.order(:area_id, :flag, :id)
      a = Area.all

      gon.area = a
      gon.pflag = pd
      gon.false = true
      @cu = current_user.profile_id

      if @cu != 0
        @per = Permission.where("view_name = 'Profiles' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarPerfil = permisos.editar

          if permisos.view_name == @@byPerfiles

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.view_name
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@crear == 8))
          a = Area.all

          gon.area = a
          gon.false = true

          @profile = Profile.new
          @views = View.all

          @area_usuario = current_user.area_id

          if @area_usuario == "1"

            areas_arr = Array.new

            @areas = Area.all

            @areas.each do |ar|
              valor = {id: '', name: '', area_id: ''}
              valor[:id] = ar.id
              valor[:name] = ar.name
              valor[:area_id] = ar.area_id

              areas_arr.push(valor)
            end

            @areas = areas_arr
          else
            @areas = Area.where("id = ?", @area_usuario)

            areas_arr = Array.new

            @areas.each do |ar|
              @nombre = ar.name
              valor = {id: '', name: '', area_id: ''}
              valor[:id] = ar.id
              valor[:name] = ar.name
              valor[:area_id] = ar.area_id

              areas_arr.push(valor)

              @subarea_1 = Area.where("area_id = ?", ar.id)

              if !@subarea_1.nil?

                @subarea_1.each do |sub|
                  valor = {id: '', name: '', area_id: ''}
                  valor[:id] = sub.id
                  valor[:name] = sub.name
                  valor[:area_id] = sub.area_id

                  areas_arr.push(valor)

                  @subarea_2 = Area.where("area_id = ?", sub.id)

                  if !@subarea_2.nil?

                    @subarea_2.each do |sub2|
                      valor = {id: '', name: '', area_id: ''}
                      valor[:id] = sub2.id
                      valor[:name] = sub2.name
                      valor[:area_id] = sub2.area_id

                      areas_arr.push(valor)

                      @subarea_3 = Area.where("area_id = ?", sub2.id)

                      if !@subarea_3.nil?

                        @subarea_3.each do |sub3|
                          valor = {id: '', name: '', area_id: ''}
                          valor[:id] = sub3.id
                          valor[:name] = sub3.name
                          valor[:area_id] = sub3.area_id

                          areas_arr.push(valor)

                          @subarea_4 = Area.where("area_id = ?", sub3.id)

                          if !@subarea_4.nil?

                            @subarea_4.each do |sub4|
                              valor = {id: '', name: '', area_id: ''}
                              valor[:id] = sub4.id
                              valor[:name] = sub4.name
                              valor[:area_id] = sub4.area_id

                              areas_arr.push(valor)

                              @subarea_5 = Area.where("area_id = ?", sub4.id)

                              if !@subarea_5.nil?

                                @subarea_5.each do |sub5|
                                  valor = {id: '', name: '', area_id: ''}
                                  valor[:id] = sub5.id
                                  valor[:name] = sub5.name
                                  valor[:area_id] = sub5.area_id

                                  areas_arr.push(valor)

                                  @subarea_6 = Area.where("area_id = ?", sub5.id)

                                  if !@subarea_6.nil?

                                    @subarea_6.each do |sub6|
                                      valor = {id: '', name: '', area_id: ''}
                                      valor[:id] = sub6.id
                                      valor[:name] = sub6.name
                                      valor[:area_id] = sub6.area_id

                                      areas_arr.push(valor)

                                      @subarea_7 = Area.where("area_id = ?", sub6.id)

                                      if !@subarea_7.nil?

                                        @subarea_7.each do |sub7|
                                          valor = {id: '', name: '', area_id: ''}
                                          valor[:id] = sub7.id
                                          valor[:name] = sub7.name
                                          valor[:area_id] = sub7.area_id

                                          areas_arr.push(valor)

                                          @subarea_8 = Area.where("area_id = ?", sub7.id)

                                          if !@subarea_8.nil?

                                            @subarea_8.each do |sub8|
                                              valor = {id: '', name: '', area_id: ''}
                                              valor[:id] = sub8.id
                                              valor[:name] = sub8.name
                                              valor[:area_id] = sub8.area_id

                                              areas_arr.push(valor)

                                              @subarea_9 = Area.where("area_id = ?", sub8.id)

                                              if !@subarea_9.nil?

                                                @subarea_9.each do |sub9|
                                                  valor = {id: '', name: '', area_id: ''}
                                                  valor[:id] = sub9.id
                                                  valor[:name] = sub9.name
                                                  valor[:area_id] = sub9.area_id

                                                  areas_arr.push(valor)

                                                  @subarea_10 = Area.where("area_id = ?", sub9.id)

                                                  if !@subarea_10.nil?

                                                    @subarea_10.each do |sub10|
                                                      valor = {id: '', name: '', area_id: ''}
                                                      valor[:id] = sub10.id
                                                      valor[:name] = sub10.name
                                                      valor[:area_id] = sub10.area_id

                                                      areas_arr.push(valor)
                                                    end
                                                  end
                                                end
                                              end
                                            end
                                          end
                                        end
                                      end
                                    end
                                  end
                                end
                              end
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end
            end

            @areas = areas_arr

          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end

  end

  # GET /profiles/1/edit
  def edit
    if current_user
      pd = Profile.all.order(:area_id, :flag, :id)
      a = Area.all

      gon.area = a
      gon.pflag = pd
      gon.false = true
      @boton = true

      @cu = current_user.profile_id
      if @cu != 0

        @per = Permission.where("view_name = 'Profiles' and profile_id = ?", @cu)

        @per.each do |permisos|
          @leerPerfil = permisos.leer

          if permisos.view_name == @@byPerfiles

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        #-------------------Armado de Perfiles disponibles para el usuario de areas

        @area_usuario = current_user.area_id

        if @area_usuario == "1"

          areas_arr = Array.new

          @areas = Area.all

          @areas.each do |ar|
            valor = {id: '', name: '', area_id: ''}
            valor[:id] = ar.id
            valor[:name] = ar.name
            valor[:area_id] = ar.area_id

            areas_arr.push(valor)
          end

          @areas = areas_arr
        else
          @areas = Area.where("id = ?", @area_usuario)

          areas_arr = Array.new
          perfiles_arr = Array.new

          @areas.each do |ar|
            @nombre = ar.name
            valor = {id: '', name: '', area_id: ''}
            valor[:id] = ar.id
            valor[:name] = ar.name
            valor[:area_id] = ar.area_id

            areas_arr.push(valor)

            if ar.id == 1
              @perfiles = Profile.where("(area_id = ?) and (flag != 2) and (area_id != 1 or flag != 1)", ar.id)
            else #CUALQUIER OTRA ÁREA, MUESTRA SÓLO LA INFORMACIÓN DEL ÁREA.
              @perfiles = Profile.where("(area_id = ?) and (flag != 2)", ar.id)
            end

            @perfiles.each do |per|
              registro = {id: '', name: '', flag: '', area_id: ''}
              registro[:id] = per.id
              registro[:name] = per.name
              registro[:flag] = per.flag
              registro[:area_id] = per.area_id

              perfiles_arr.push(registro)
            end

            @subarea_1 = Area.where("area_id = ?", ar.id)

            if !@subarea_1.nil?

              @subarea_1.each do |sub|
                valor = {id: '', name: '', area_id: ''}
                valor[:id] = sub.id
                valor[:name] = sub.name
                valor[:area_id] = sub.area_id

                areas_arr.push(valor)

                @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub.id)

                @perfiles.each do |per|
                  registro = {id: '', name: '', flag: '', area_id: ''}
                  registro[:id] = per.id
                  registro[:name] = per.name
                  registro[:flag] = per.flag
                  registro[:area_id] = per.area_id

                  perfiles_arr.push(registro)
                end

                @subarea_2 = Area.where("area_id = ?", sub.id)

                if !@subarea_2.nil?

                  @subarea_2.each do |sub2|
                    valor = {id: '', name: '', area_id: ''}
                    valor[:id] = sub2.id
                    valor[:name] = sub2.name
                    valor[:area_id] = sub2.area_id

                    areas_arr.push(valor)

                    @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub2.id)

                    @perfiles.each do |per|
                      registro = {id: '', name: '', flag: '', area_id: ''}
                      registro[:id] = per.id
                      registro[:name] = per.name
                      registro[:flag] = per.flag
                      registro[:area_id] = per.area_id

                      perfiles_arr.push(registro)
                    end

                    @subarea_3 = Area.where("area_id = ?", sub2.id)

                    if !@subarea_3.nil?

                      @subarea_3.each do |sub3|
                        valor = {id: '', name: '', area_id: ''}
                        valor[:id] = sub3.id
                        valor[:name] = sub3.name
                        valor[:area_id] = sub3.area_id

                        areas_arr.push(valor)

                        @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub3.id)

                        @perfiles.each do |per|
                          registro = {id: '', name: '', flag: '', area_id: ''}
                          registro[:id] = per.id
                          registro[:name] = per.name
                          registro[:flag] = per.flag
                          registro[:area_id] = per.area_id

                          perfiles_arr.push(registro)
                        end

                        @subarea_4 = Area.where("area_id = ?", sub3.id)

                        if !@subarea_4.nil?

                          @subarea_4.each do |sub4|
                            valor = {id: '', name: '', area_id: ''}
                            valor[:id] = sub4.id
                            valor[:name] = sub4.name
                            valor[:area_id] = sub4.area_id

                            areas_arr.push(valor)

                            @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub4.id)

                            @perfiles.each do |per|
                              registro = {id: '', name: '', flag: '', area_id: ''}
                              registro[:id] = per.id
                              registro[:name] = per.name
                              registro[:flag] = per.flag
                              registro[:area_id] = per.area_id

                              perfiles_arr.push(registro)
                            end

                            @subarea_5 = Area.where("area_id = ?", sub4.id)

                            if !@subarea_5.nil?

                              @subarea_5.each do |sub5|
                                valor = {id: '', name: '', area_id: ''}
                                valor[:id] = sub5.id
                                valor[:name] = sub5.name
                                valor[:area_id] = sub5.area_id

                                areas_arr.push(valor)

                                @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub5.id)

                                @perfiles.each do |per|
                                  registro = {id: '', name: '', flag: '', area_id: ''}
                                  registro[:id] = per.id
                                  registro[:name] = per.name
                                  registro[:flag] = per.flag
                                  registro[:area_id] = per.area_id

                                  perfiles_arr.push(registro)
                                end

                                @subarea_6 = Area.where("area_id = ?", sub5.id)

                                if !@subarea_6.nil?

                                  @subarea_6.each do |sub6|
                                    valor = {id: '', name: '', area_id: ''}
                                    valor[:id] = sub6.id
                                    valor[:name] = sub6.name
                                    valor[:area_id] = sub6.area_id

                                    areas_arr.push(valor)

                                    @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub6.id)

                                    @perfiles.each do |per|
                                      registro = {id: '', name: '', flag: '', area_id: ''}
                                      registro[:id] = per.id
                                      registro[:name] = per.name
                                      registro[:flag] = per.flag
                                      registro[:area_id] = per.area_id

                                      perfiles_arr.push(registro)
                                    end

                                    @subarea_7 = Area.where("area_id = ?", sub6.id)

                                    if !@subarea_7.nil?

                                      @subarea_7.each do |sub7|
                                        valor = {id: '', name: '', area_id: ''}
                                        valor[:id] = sub7.id
                                        valor[:name] = sub7.name
                                        valor[:area_id] = sub7.area_id

                                        areas_arr.push(valor)

                                        @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub7.id)

                                        @perfiles.each do |per|
                                          registro = {id: '', name: '', flag: '', area_id: ''}
                                          registro[:id] = per.id
                                          registro[:name] = per.name
                                          registro[:flag] = per.flag
                                          registro[:area_id] = per.area_id

                                          perfiles_arr.push(registro)
                                        end

                                        @subarea_8 = Area.where("area_id = ?", sub7.id)

                                        if !@subarea_8.nil?

                                          @subarea_8.each do |sub8|
                                            valor = {id: '', name: '', area_id: ''}
                                            valor[:id] = sub8.id
                                            valor[:name] = sub8.name
                                            valor[:area_id] = sub8.area_id

                                            areas_arr.push(valor)

                                            @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub8.id)

                                            @perfiles.each do |per|
                                              registro = {id: '', name: '', flag: '', area_id: ''}
                                              registro[:id] = per.id
                                              registro[:name] = per.name
                                              registro[:flag] = per.flag
                                              registro[:area_id] = per.area_id

                                              perfiles_arr.push(registro)
                                            end

                                            @subarea_9 = Area.where("area_id = ?", sub8.id)

                                            if !@subarea_9.nil?

                                              @subarea_9.each do |sub9|
                                                valor = {id: '', name: '', area_id: ''}
                                                valor[:id] = sub9.id
                                                valor[:name] = sub9.name
                                                valor[:area_id] = sub9.area_id

                                                areas_arr.push(valor)

                                                @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub9.id)

                                                @perfiles.each do |per|
                                                  registro = {id: '', name: '', flag: '', area_id: ''}
                                                  registro[:id] = per.id
                                                  registro[:name] = per.name
                                                  registro[:flag] = per.flag
                                                  registro[:area_id] = per.area_id

                                                  perfiles_arr.push(registro)
                                                end

                                                @subarea_10 = Area.where("area_id = ?", sub9.id)

                                                if !@subarea_10.nil?

                                                  @subarea_10.each do |sub10|
                                                    valor = {id: '', name: '', area_id: ''}
                                                    valor[:id] = sub10.id
                                                    valor[:name] = sub10.name
                                                    valor[:area_id] = sub10.area_id

                                                    areas_arr.push(valor)

                                                    @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub10.id)

                                                    @perfiles.each do |per|
                                                      registro = {id: '', name: '', flag: '', area_id: ''}
                                                      registro[:id] = per.id
                                                      registro[:name] = per.name
                                                      registro[:flag] = per.flag
                                                      registro[:area_id] = per.area_id

                                                      perfiles_arr.push(registro)
                                                    end
                                                  end
                                                end
                                              end
                                            end
                                          end
                                        end
                                      end
                                    end
                                  end
                                end
                              end
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end
            end
          end

          @areas = areas_arr

        end

        #-------------------Fin de Armado

        if @@editar == 4 && ((@profile.flag != 2) && (@profile.flag != 1 || @profile.area_id != 1))
          @views = View.all
          if current_user.profile.flag == 0 #Es un administrador central
            @id = params[:id]

            a = Area.all

            gon.area = a
            gon.false = true
            @profile = Profile.find(params[:id])
            @edit = Permission.where("profile_id = ?", @id)

          elsif current_user.profile.flag == 1 # Es un administrador de área
            @id = params[:id]

            @visible = false

            perfiles_arr.each do |per_visible|
              @id_actual = per_visible[:id]
              if @id_actual == @id.to_i
                @visible = true
                break
              else
                @visible = false
              end
            end

            if @visible == true
              if (@profile.area_id == current_user.area_id.to_i) && ((@profile.flag != 1) && (@profile.flag != 0))
                a = Area.all

                gon.area = a
                gon.false = true
                @profile = Profile.find(params[:id])
                @edit = Permission.where("profile_id = ?", @id)
              elsif (@profile.flag >= current_user.profile.flag && @profile.area_id > current_user.area_id.to_i)
                @id = params[:id]

                a = Area.all

                gon.area = a
                gon.false = true
                @profile = Profile.find(params[:id])
                @edit = Permission.where("profile_id = ?", @id)
              else
                @Without_Permission = 100
                redirect_to home_index_path, :alert => t('all.not_access')
              end
            else
              @Without_Permission = 100
              redirect_to home_index_path, :alert => t('all.not_access')
            end

          else # Es cualquier otro perfil
            @id = params[:id]

            @visible = false

            perfiles_arr.each do |per_visible|
              @id_actual = per_visible[:id]
              if @id_actual == @id.to_i
                @visible = true
                break
              else
                @visible = false
              end
            end

            if @visible == true
              if (@profile.area_id == current_user.area_id.to_i) && ((@profile.flag != 1) && (@profile.flag != 0) && (@profile.flag > current_user.profile.flag))
                a = Area.all

                gon.area = a
                gon.false = true
                @profile = Profile.find(params[:id])
                @edit = Permission.where("profile_id = ?", @id)
              elsif (@profile.flag >= current_user.profile.flag && @profile.area_id > current_user.area_id.to_i)
                @id = params[:id]

                a = Area.all

                gon.area = a
                gon.false = true
                @profile = Profile.find(params[:id])
                @edit = Permission.where("profile_id = ?", @id)
              else
                @Without_Permission = 100
                redirect_to home_index_path, :alert => t('all.not_access')
              end
            else
              @Without_Permission = 100
              redirect_to home_index_path, :alert => t('all.not_access')
            end
          end

        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # POST /profiles
  # POST /profiles.json
  def create
    pd = Profile.all
    a = Area.all

    gon.area = a
    gon.pflag = pd
    gon.false = true
    if current_user
      @page = "Profiles"
      @user = current_user.name
      @user_id = current_user.id
      @userl = current_user.last_name
      @action = "1"
      @account = "not yet"
      @lasac = "not available"
      @cli_id = 0
      #@det = @loyalty_client.params[:id]
      @date = Time.now

      # @historic = Historic.new
      # @historic.page = @page
      # @historic.user_name = @user + " " + @userl
      # @historic.user_id = @user_id
      # #@historic.action = @det
      # @historic.action = @action
      # @historic.account = @account
      # @historic.last_account = @lasac
      # @historic.client_id = @cli_id
      # @historic.date = @date

      @perfil_existente = Profile.find_by(name: params[:profile][:name], area_id: params[:profile][:area_id])

      if @perfil_existente.nil?
        @profile = Profile.new(profile_params)

        respond_to do |format|

          if @profile.save

            @id_perfil = Profile.find_by_id(params[:profile][:flag])

            if @id_perfil.flag == 0 || @id_perfil.flag == 1 #Administrador Central o Administrador de Área
              @profile.flag = 3
              @profile.save
            elsif @id_perfil.flag >= 3 #Cualquier otro perfil
              @profile.flag = @id_perfil.flag + 1
              @profile.save
            end

            # @historic.detail = @profile.to_json
            # @historic.save

            @area = Area.find_by_id(@profile.area_id)

            @views = View.all

            @views.each do |v|
              @permission = Permission.new
              @permission.view_id = v.id
              @permission.view_name = v.name


              @crear = params["crear"+v.id.to_s]
              @editar = params["editar"+v.id.to_s]
              @leer = params["leer"+v.id.to_s]
              @eliminar = params["eliminar"+v.id.to_s]

              if @crear == "8"
                @permission.crear = @crear.to_i
              end

              if @editar == "4"
                @permission.editar = @editar.to_i
              end

              if @leer == "2"
                @permission.leer = @leer.to_i
              end

              if @eliminar == "1"
                @permission.eliminar = @eliminar.to_i
              end

              @permission.profile_id = @profile.id
              @permission.save
            end

            # Plantilla para agregar más permisos
            # @permission = Permission.new
            # @permission.view_id = params[:name6].to_i
            # @permission.view_name = View.find(params[:name6].to_i).name
            # @permission.crear = params[:crear6]
            # @permission.editar = params[:editar6]
            # @permission.leer = params[:leer6]
            # @permission.eliminar = params[:eliminar6]
            # @permission.profile_id = @profile.id
            # @permission.save


            if @profile.save
              format.html {redirect_to profiles_path, notice: t('views.mess_cont_per_sus')}
              format.json {render :show, status: :created, location: @profile}
            else
              format.html {render :new}
              format.json {render json: @profile.errors, status: :unprocessable_entity}

            end
          end
        end
      else
        redirect_to profiles_path, :alert => t('activerecord.atributes.per_not_saved')
      end

    else
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # PATCH/PUT /profiles/1
  # PATCH/PUT /profiles/1.json
  def update
    pd = Profile.all
    a = Area.all

    gon.area = a
    gon.pflag = pd
    gon.false = true
    if current_user
      @page = "Profiles"
      @user = current_user.name
      @user_id = current_user.id
      @userl = current_user.last_name
      @action = "2"
      @account = "not yet"
      @lasac = "not available"
      @cli_id = 0
      #@det = @loyalty_client.params[:id]
      @date = Time.now

      # @historic = Historic.new
      # @historic.page = @page
      # @historic.user_name = @user + " " + @userl
      # @historic.user_id = @user_id
      # #@historic.action = @det
      # @historic.action = @action
      # @historic.account = @account
      # @historic.last_account = @lasac
      # @historic.client_id = @cli_id
      # @historic.date = @date
      # #@historic.save

      @perfil_existente = Profile.find_by(name: params[:profile][:name], area_id: params[:profile][:area_id])

      @profile_area = Profile.find(params[:id])

      @profile_area = @profile_area.area_id

      if @perfil_existente.nil?
        respond_to do |format|
          @area = params[:profile][:area_id]

          if @profile.update(profile_params)
            # @historic.detail = @profile.to_json
            # @historic.save

            if @area.nil? || @area == ""
              @profile.area_id = @profile_area
              @profile.save
            end

            @area = Area.find_by_id(@profile.area_id)
            @perfil = Profile.find_by_id(@profile.id)

            if params[:profile][:flag].present?
              @perfil_dep = Profile.find_by_id(params[:profile][:flag])

              if @perfil_dep.flag == 0 || @perfil_dep.flag == 1
                @perfil.flag = 3
              else
                @perfil.flag = @perfil_dep.flag + 1
              end
            end
            @perfil.save

            @views = View.all

            @views.each do |v|
              @permission = Permission.find_by(view_id: v.id, profile_id: @profile.id)
              @permission.view_id = v.id
              @permission.view_name = v.name


              @crear_per = params["crear"+v.id.to_s]
              @editar_per = params["editar"+v.id.to_s]
              @leer_per = params["leer"+v.id.to_s]
              @eliminar_per = params["eliminar"+v.id.to_s]

              if @crear_per == "8"
                @permission.crear = @crear_per.to_i
              else
                @permission.crear = nil
              end

              if @editar_per == "4"
                @permission.editar = @editar_per.to_i
              else
                @permission.editar = nil
              end

              if @leer_per == "2"
                @permission.leer = @leer_per.to_i
              else
                @permission.leer = nil
              end

              if @eliminar_per == "1"
                @permission.eliminar = @eliminar_per.to_i
              else
                @permission.eliminar = nil
              end

              @permission.profile_id = @profile.id
              @permission.save
            end

            # # Plantilla para nuevos permisos
            # @name6 = View.find(params[:name6].to_i).name
            # @permission6 = Permission.where("view_name = '"+ @name6 +"' and profile_id = " + params[:id])
            # @permission6.each do |f|
            #   f.update_attribute(:crear, params[:crear6])
            #   f.update_attribute(:editar, params[:editar6])
            #   f.update_attribute(:leer, params[:leer6])
            #   f.update_attribute(:eliminar, params[:eliminar6])
            # end

            @users = User.where("profile_id = ?", @profile.id)

            @users.each do |us|
              us.area_id = @profile.area_id
              us.save
            end

            format.html {redirect_to @profile, notice: t('views.mess_cont_per_up')}
            format.json {render :show, status: :ok, location: @profile}

          else
            format.html {render :edit}
            format.json {render json: @profile.errors, status: :unprocessable_entity}
          end
        end
      else
        redirect_to profiles_path, :alert => t('activerecord.atributes.per_not_updated')
      end

    else
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # DELETE /profiles/1
  # DELETE /profiles/1.json
  def destroy
    if current_user
      @page = "Profiles"
      @user = current_user.name
      @user_id = current_user.id
      @userl = current_user.last_name
      @action = "4"
      @account = "not yet"
      @lasac = "not available"
      @cli_id = 0
      #@det = @loyalty_client.params[:id]
      @date = Time.now

      # @historic = Historic.new
      # @historic.page = @page
      # @historic.user_name = @user + " " + @userl
      # @historic.user_id = @user_id
      # #@historic.action = @det
      # @historic.action = @action
      # @historic.account = @account
      # @historic.last_account = @lasac
      # @historic.client_id = @cli_id
      # @historic.date = @date
      # #@historic.save

      @cu = current_user.profile_id
      if @cu != 0

        @per = Permission.where("view_name = 'Profiles' and profile_id = ?", @cu)

        @per.each do |permisos|
          @leerPerfil = permisos.leer

          if permisos.view_name == @@byPerfiles

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.view_name
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        #-------------------Armado de Perfiles disponibles para el usuario de areas

        @area_usuario = current_user.area_id

        if @area_usuario != "1"
          @areas = Area.where("id = ?", @area_usuario)

          areas_arr = Array.new
          perfiles_arr = Array.new

          @areas.each do |ar|
            @nombre = ar.name
            valor = {id: '', name: '', area_id: ''}
            valor[:id] = ar.id
            valor[:name] = ar.name
            valor[:area_id] = ar.area_id

            areas_arr.push(valor)

            if ar.id == 1
              @perfiles = Profile.where("(area_id = ?) and (flag != 2) and (area_id != 1 or flag != 1)", ar.id)
            else #CUALQUIER OTRA ÁREA, MUESTRA SÓLO LA INFORMACIÓN DEL ÁREA.
              @perfiles = Profile.where("(area_id = ?) and (flag != 2)", ar.id)
            end

            @perfiles.each do |per|
              registro = {id: '', name: '', flag: '', area_id: ''}
              registro[:id] = per.id
              registro[:name] = per.name
              registro[:flag] = per.flag
              registro[:area_id] = per.area_id

              perfiles_arr.push(registro)
            end

            @subarea_1 = Area.where("area_id = ?", ar.id)

            if !@subarea_1.nil?

              @subarea_1.each do |sub|
                valor = {id: '', name: '', area_id: ''}
                valor[:id] = sub.id
                valor[:name] = sub.name
                valor[:area_id] = sub.area_id

                areas_arr.push(valor)

                @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub.id)

                @perfiles.each do |per|
                  registro = {id: '', name: '', flag: '', area_id: ''}
                  registro[:id] = per.id
                  registro[:name] = per.name
                  registro[:flag] = per.flag
                  registro[:area_id] = per.area_id

                  perfiles_arr.push(registro)
                end

                @subarea_2 = Area.where("area_id = ?", sub.id)

                if !@subarea_2.nil?

                  @subarea_2.each do |sub2|
                    valor = {id: '', name: '', area_id: ''}
                    valor[:id] = sub2.id
                    valor[:name] = sub2.name
                    valor[:area_id] = sub2.area_id

                    areas_arr.push(valor)

                    @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub2.id)

                    @perfiles.each do |per|
                      registro = {id: '', name: '', flag: '', area_id: ''}
                      registro[:id] = per.id
                      registro[:name] = per.name
                      registro[:flag] = per.flag
                      registro[:area_id] = per.area_id

                      perfiles_arr.push(registro)
                    end

                    @subarea_3 = Area.where("area_id = ?", sub2.id)

                    if !@subarea_3.nil?

                      @subarea_3.each do |sub3|
                        valor = {id: '', name: '', area_id: ''}
                        valor[:id] = sub3.id
                        valor[:name] = sub3.name
                        valor[:area_id] = sub3.area_id

                        areas_arr.push(valor)

                        @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub3.id)

                        @perfiles.each do |per|
                          registro = {id: '', name: '', flag: '', area_id: ''}
                          registro[:id] = per.id
                          registro[:name] = per.name
                          registro[:flag] = per.flag
                          registro[:area_id] = per.area_id

                          perfiles_arr.push(registro)
                        end

                        @subarea_4 = Area.where("area_id = ?", sub3.id)

                        if !@subarea_4.nil?

                          @subarea_4.each do |sub4|
                            valor = {id: '', name: '', area_id: ''}
                            valor[:id] = sub4.id
                            valor[:name] = sub4.name
                            valor[:area_id] = sub4.area_id

                            areas_arr.push(valor)

                            @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub4.id)

                            @perfiles.each do |per|
                              registro = {id: '', name: '', flag: '', area_id: ''}
                              registro[:id] = per.id
                              registro[:name] = per.name
                              registro[:flag] = per.flag
                              registro[:area_id] = per.area_id

                              perfiles_arr.push(registro)
                            end

                            @subarea_5 = Area.where("area_id = ?", sub4.id)

                            if !@subarea_5.nil?

                              @subarea_5.each do |sub5|
                                valor = {id: '', name: '', area_id: ''}
                                valor[:id] = sub5.id
                                valor[:name] = sub5.name
                                valor[:area_id] = sub5.area_id

                                areas_arr.push(valor)

                                @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub5.id)

                                @perfiles.each do |per|
                                  registro = {id: '', name: '', flag: '', area_id: ''}
                                  registro[:id] = per.id
                                  registro[:name] = per.name
                                  registro[:flag] = per.flag
                                  registro[:area_id] = per.area_id

                                  perfiles_arr.push(registro)
                                end

                                @subarea_6 = Area.where("area_id = ?", sub5.id)

                                if !@subarea_6.nil?

                                  @subarea_6.each do |sub6|
                                    valor = {id: '', name: '', area_id: ''}
                                    valor[:id] = sub6.id
                                    valor[:name] = sub6.name
                                    valor[:area_id] = sub6.area_id

                                    areas_arr.push(valor)

                                    @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub6.id)

                                    @perfiles.each do |per|
                                      registro = {id: '', name: '', flag: '', area_id: ''}
                                      registro[:id] = per.id
                                      registro[:name] = per.name
                                      registro[:flag] = per.flag
                                      registro[:area_id] = per.area_id

                                      perfiles_arr.push(registro)
                                    end

                                    @subarea_7 = Area.where("area_id = ?", sub6.id)

                                    if !@subarea_7.nil?

                                      @subarea_7.each do |sub7|
                                        valor = {id: '', name: '', area_id: ''}
                                        valor[:id] = sub7.id
                                        valor[:name] = sub7.name
                                        valor[:area_id] = sub7.area_id

                                        areas_arr.push(valor)

                                        @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub7.id)

                                        @perfiles.each do |per|
                                          registro = {id: '', name: '', flag: '', area_id: ''}
                                          registro[:id] = per.id
                                          registro[:name] = per.name
                                          registro[:flag] = per.flag
                                          registro[:area_id] = per.area_id

                                          perfiles_arr.push(registro)
                                        end

                                        @subarea_8 = Area.where("area_id = ?", sub7.id)

                                        if !@subarea_8.nil?

                                          @subarea_8.each do |sub8|
                                            valor = {id: '', name: '', area_id: ''}
                                            valor[:id] = sub8.id
                                            valor[:name] = sub8.name
                                            valor[:area_id] = sub8.area_id

                                            areas_arr.push(valor)

                                            @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub8.id)

                                            @perfiles.each do |per|
                                              registro = {id: '', name: '', flag: '', area_id: ''}
                                              registro[:id] = per.id
                                              registro[:name] = per.name
                                              registro[:flag] = per.flag
                                              registro[:area_id] = per.area_id

                                              perfiles_arr.push(registro)
                                            end

                                            @subarea_9 = Area.where("area_id = ?", sub8.id)

                                            if !@subarea_9.nil?

                                              @subarea_9.each do |sub9|
                                                valor = {id: '', name: '', area_id: ''}
                                                valor[:id] = sub9.id
                                                valor[:name] = sub9.name
                                                valor[:area_id] = sub9.area_id

                                                areas_arr.push(valor)

                                                @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub9.id)

                                                @perfiles.each do |per|
                                                  registro = {id: '', name: '', flag: '', area_id: ''}
                                                  registro[:id] = per.id
                                                  registro[:name] = per.name
                                                  registro[:flag] = per.flag
                                                  registro[:area_id] = per.area_id

                                                  perfiles_arr.push(registro)
                                                end

                                                @subarea_10 = Area.where("area_id = ?", sub9.id)

                                                if !@subarea_10.nil?

                                                  @subarea_10.each do |sub10|
                                                    valor = {id: '', name: '', area_id: ''}
                                                    valor[:id] = sub10.id
                                                    valor[:name] = sub10.name
                                                    valor[:area_id] = sub10.area_id

                                                    areas_arr.push(valor)

                                                    @perfiles = Profile.where("(area_id = ?) and (flag != 2)", sub10.id)

                                                    @perfiles.each do |per|
                                                      registro = {id: '', name: '', flag: '', area_id: ''}
                                                      registro[:id] = per.id
                                                      registro[:name] = per.name
                                                      registro[:flag] = per.flag
                                                      registro[:area_id] = per.area_id

                                                      perfiles_arr.push(registro)
                                                    end
                                                  end
                                                end
                                              end
                                            end
                                          end
                                        end
                                      end
                                    end
                                  end
                                end
                              end
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end
            end
          end
        end

        #-------------------Fin de Armado

        if @@eliminar == 1 && ((@profile.flag != 0) && (@profile.flag != 1) && (@profile.flag != 2))
          if current_user.profile.flag == 0 #Es un administrador central
            @name_profile.destroy
            @user = User.where("nameProfile_id = ?", @name_profile.id)
            @permisos = Profile.where("nameProfile_id = ?", @name_profile.id)

            if @user.present?
              @user.each do |u|
                u.destroy
              end
            end

            @permisos.each do |per|
              per.destroy
            end
          elsif current_user.profile.flag == 1 # Es un administrador de área
            @id = params[:id]

            @visible = false

            perfiles_arr.each do |per_visible|
              @id_actual = per_visible[:id]
              if @id_actual == @id.to_i
                @visible = true
                break
              else
                @visible = false
              end
            end

            if @visible == true
              if (@profile.area_id == current_user.area_id.to_i) && ((@profile.flag != 1) && (@profile.flag != 0))
                @profile.destroy
                @user = User.where("profile_id = ?", @profile.id)
                @permisos = Permission.where("profile_id = ?", @profile.id)

                if @user.present?
                  @user.each do |u|
                    u.destroy
                  end
                end

                @permisos.each do |per|
                  per.destroy
                end
              elsif (@profile.flag >= current_user.profile.flag && @profile.area_id > current_user.area_id.to_i)
                @profile.destroy
                @user = User.where("profile_id = ?", @profile.id)
                @permisos = Permission.where("profile_id = ?", @profile.id)

                if @user.present?
                  @user.each do |u|
                    u.destroy
                  end
                end

                @permisos.each do |per|
                  per.destroy
                end
              else
                @Without_Permission = 100
                redirect_to home_index_path, :alert => t('all.not_access')
              end
            else
              @Without_Permission = 100
              redirect_to home_index_path, :alert => t('all.not_access')
            end

          else # Es cualquier otro perfil
            @id = params[:id]

            @visible = false

            perfiles_arr.each do |per_visible|
              @id_actual = per_visible[:id]
              if @id_actual == @id.to_i
                @visible = true
                break
              else
                @visible = false
              end
            end

            if @visible == true
              if (@profile.area_id == current_user.area_id.to_i) && ((@profile.flag != 1) && (@profile.flag != 0) && (@profile.flag > current_user.profile.flag))
                @profile.destroy
                @user = User.where("profile_id = ?", @profile.id)
                @permisos = Permission.where("profile_id = ?", @profile.id)

                if @user.present?
                  @user.each do |u|
                    u.destroy
                  end
                end

                @permisos.each do |per|
                  per.destroy
                end
              elsif (@profile.flag >= current_user.profile.flag && @profile.area_id > current_user.area_id.to_i)
                @profile.destroy
                @user = User.where("profile_id = ?", @profile.id)
                @permisos = Permission.where("profile_id = ?", @profile.id)

                if @user.present?
                  @user.each do |u|
                    u.destroy
                  end
                end

                @permisos.each do |per|
                  per.destroy
                end
              else
                @Without_Permission = 100
                redirect_to home_index_path, :alert => t('all.not_access')
              end
            else
              @Without_Permission = 100
              redirect_to home_index_path, :alert => t('all.not_access')
            end
          end

          respond_to do |format|
            # @historic.detail = @name_profile.to_json
            # @historic.save
            format.html {redirect_to profiles_url, notice: t('activerecord.atributes.per_del')}
            format.json {head :no_content}
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end

      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_profile
    @profile = Profile.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def profile_params
    params.require(:profile).permit(:name, :flag, :area_id)
  end
end
