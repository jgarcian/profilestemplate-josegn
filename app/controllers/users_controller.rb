class UsersController < ApplicationController
  before_filter :authenticate_user!

  @@user = "Users"
  @@byUsers = "Users"

  def index
    if current_user
      @usersa = User.all

      @page = "Users"
      @user = current_user.name
      @user_id = current_user.id
      @userl = current_user.last_name
      @action = "3"
      @account = "not yet"
      @lasac = "not available"
      @cli_id = 0
      @description = User.all.count.to_i
      @date = Time.now

      # @historic = Historic.new
      # @historic.page = @page
      # @historic.user_name = @user + " " + @userl
      # @historic.user_id = @user_id
      # @historic.action = @action
      # @historic.account = @account
      # @historic.last_account = @lasac
      # @historic.client_id = @cli_id
      # @historic.date = @date
      # @historic.description = @description
      # @historic.save

      pd = Profile.all
      a = Area.all

      gon.area = a
      gon.pflag = pd
      gon.false = true

      @cu= current_user.profile_id

      if @cu !=0
        @per = Permission.where("view_name = 'Users' and profile_id= ?", @cu)

        @per.each do |permisos|
          @name = permisos.view_name
          @crearUser = permisos.crear
          @editarUser = permisos.editar
          @leerUser = permisos.leer
          @eliminarUser = permisos.eliminar
          # @fla = permisos.Profile.flag
          # @ar = permisos.Profile.area_id

          if @name == @@byUsers
            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.view_name
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end
        end

        if ((@@crear == 8) || (@@editar == 4) || (@@leer == 2) || (@@eliminar == 1)) #|| (@fla >= current_user.Profile.flag)

          @area_usuario = current_user.area_id

          ## ------------------------------------ Usuarios permitidos de acuerdo a las áreas y subáreas hasta 10 niveles abajo de donde se encuentra el usuario
          if @area_usuario == "1" #------------------ Si es de central le muestro todos los usuarios de todas las áreas
            @users = User.where("profile_id IS NOT NULL and area_id IS NOT NULL").page(params[:page]).per(10)
          else #------------------------------------- Si no es de central busco los usuarios que deberán mostrarse de acuerdo a su área y subáreas
            @areas = Area.where("id = ?", @area_usuario)

            areas_arr = Array.new
            usuarios_arr = Array.new

            @areas.each do |ar|
              @nombre = ar.name
              valor = {id: '', name: '', area_id: ''}
              valor[:id] = ar.id
              valor[:name] = ar.name
              valor[:area_id] = ar.area_id

              areas_arr.push(valor)

              @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", ar.id)

              @usuarios.each do |usu|
                registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                registro[:id] = usu.id
                registro[:email] = usu.email
                registro[:user_name] = usu.user_name
                registro[:name] = usu.name
                registro[:last_name] = usu.last_name
                registro[:birthday] = usu.birthday
                registro[:boss_name] = usu.boss_name
                registro[:phone] = usu.phone
                registro[:ext] = usu.ext
                registro[:profile_id] = usu.profile_id
                registro[:area_id] = usu.area_id
                registro[:employment] = usu.employment
                registro[:activo]= usu.activo

                usuarios_arr.push(registro)
              end

              @subarea_1 = Area.where("area_id = ?", ar.id)

              if !@subarea_1.nil?

                @subarea_1.each do |sub|
                  valor = {id: '', name: '', area_id: ''}
                  valor[:id] = sub.id
                  valor[:name] = sub.name
                  valor[:area_id] = sub.area_id

                  areas_arr.push(valor)

                  @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub.id)

                  @usuarios.each do |usu|
                    registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                    registro[:id] = usu.id
                    registro[:email] = usu.email
                    registro[:user_name] = usu.user_name
                    registro[:name] = usu.name
                    registro[:last_name] = usu.last_name
                    registro[:birthday] = usu.birthday
                    registro[:boss_name] = usu.boss_name
                    registro[:phone] = usu.phone
                    registro[:ext] = usu.ext
                    registro[:profile_id] = usu.profile_id
                    registro[:area_id] = usu.area_id
                    registro[:employment] = usu.employment
                    registro[:activo]= usu.activo

                    usuarios_arr.push(registro)
                  end

                  @subarea_2 = Area.where("area_id = ?", sub.id)

                  if !@subarea_2.nil?

                    @subarea_2.each do |sub2|
                      valor = {id: '', name: '', area_id: ''}
                      valor[:id] = sub2.id
                      valor[:name] = sub2.name
                      valor[:area_id] = sub2.area_id

                      areas_arr.push(valor)

                      @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub2.id)

                      @usuarios.each do |usu|
                        registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                        registro[:id] = usu.id
                        registro[:email] = usu.email
                        registro[:user_name] = usu.user_name
                        registro[:name] = usu.name
                        registro[:last_name] = usu.last_name
                        registro[:birthday] = usu.birthday
                        registro[:boss_name] = usu.boss_name
                        registro[:phone] = usu.phone
                        registro[:ext] = usu.ext
                        registro[:profile_id] = usu.profile_id
                        registro[:area_id] = usu.area_id
                        registro[:employment] = usu.employment
                        registro[:activo]= usu.activo

                        usuarios_arr.push(registro)
                      end

                      @subarea_3 = Area.where("area_id = ?", sub2.id)

                      if !@subarea_3.nil?

                        @subarea_3.each do |sub3|
                          valor = {id: '', name: '', area_id: ''}
                          valor[:id] = sub3.id
                          valor[:name] = sub3.name
                          valor[:area_id] = sub3.area_id

                          areas_arr.push(valor)

                          @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub3.id)

                          @usuarios.each do |usu|
                            registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                            registro[:id] = usu.id
                            registro[:email] = usu.email
                            registro[:user_name] = usu.user_name
                            registro[:name] = usu.name
                            registro[:last_name] = usu.last_name
                            registro[:birthday] = usu.birthday
                            registro[:boss_name] = usu.boss_name
                            registro[:phone] = usu.phone
                            registro[:ext] = usu.ext
                            registro[:profile_id] = usu.profile_id
                            registro[:area_id] = usu.area_id
                            registro[:employment] = usu.employment
                            registro[:activo]= usu.activo

                            usuarios_arr.push(registro)
                          end

                          @subarea_4 = Area.where("area_id = ?", sub3.id)

                          if !@subarea_4.nil?

                            @subarea_4.each do |sub4|
                              valor = {id: '', name: '', area_id: ''}
                              valor[:id] = sub4.id
                              valor[:name] = sub4.name
                              valor[:area_id] = sub4.area_id

                              areas_arr.push(valor)

                              @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub4.id)

                              @usuarios.each do |usu|
                                registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                registro[:id] = usu.id
                                registro[:email] = usu.email
                                registro[:user_name] = usu.user_name
                                registro[:name] = usu.name
                                registro[:last_name] = usu.last_name
                                registro[:birthday] = usu.birthday
                                registro[:boss_name] = usu.boss_name
                                registro[:phone] = usu.phone
                                registro[:ext] = usu.ext
                                registro[:profile_id] = usu.profile_id
                                registro[:area_id] = usu.area_id
                                registro[:employment] = usu.employment
                                registro[:activo]= usu.activo

                                usuarios_arr.push(registro)
                              end

                              @subarea_5 = Area.where("area_id = ?", sub4.id)

                              if !@subarea_5.nil?

                                @subarea_5.each do |sub5|
                                  valor = {id: '', name: '', area_id: ''}
                                  valor[:id] = sub5.id
                                  valor[:name] = sub5.name
                                  valor[:area_id] = sub5.area_id

                                  areas_arr.push(valor)

                                  @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub5.id)

                                  @usuarios.each do |usu|
                                    registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                    registro[:id] = usu.id
                                    registro[:email] = usu.email
                                    registro[:user_name] = usu.user_name
                                    registro[:name] = usu.name
                                    registro[:last_name] = usu.last_name
                                    registro[:birthday] = usu.birthday
                                    registro[:boss_name] = usu.boss_name
                                    registro[:phone] = usu.phone
                                    registro[:ext] = usu.ext
                                    registro[:profile_id] = usu.profile_id
                                    registro[:area_id] = usu.area_id
                                    registro[:employment] = usu.employment
                                    registro[:activo]= usu.activo

                                    usuarios_arr.push(registro)
                                  end

                                  @subarea_6 = Area.where("area_id = ?", sub5.id)

                                  if !@subarea_6.nil?

                                    @subarea_6.each do |sub6|
                                      valor = {id: '', name: '', area_id: ''}
                                      valor[:id] = sub6.id
                                      valor[:name] = sub6.name
                                      valor[:area_id] = sub6.area_id

                                      areas_arr.push(valor)

                                      @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub6.id)

                                      @usuarios.each do |usu|
                                        registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                        registro[:id] = usu.id
                                        registro[:email] = usu.email
                                        registro[:user_name] = usu.user_name
                                        registro[:name] = usu.name
                                        registro[:last_name] = usu.last_name
                                        registro[:birthday] = usu.birthday
                                        registro[:boss_name] = usu.boss_name
                                        registro[:phone] = usu.phone
                                        registro[:ext] = usu.ext
                                        registro[:profile_id] = usu.profile_id
                                        registro[:area_id] = usu.area_id
                                        registro[:employment] = usu.employment
                                        registro[:activo]= usu.activo

                                        usuarios_arr.push(registro)
                                      end

                                      @subarea_7 = Area.where("area_id = ?", sub6.id)

                                      if !@subarea_7.nil?

                                        @subarea_7.each do |sub7|
                                          valor = {id: '', name: '', area_id: ''}
                                          valor[:id] = sub7.id
                                          valor[:name] = sub7.name
                                          valor[:area_id] = sub7.area_id

                                          areas_arr.push(valor)

                                          @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub7.id)

                                          @usuarios.each do |usu|
                                            registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                            registro[:id] = usu.id
                                            registro[:email] = usu.email
                                            registro[:user_name] = usu.user_name
                                            registro[:name] = usu.name
                                            registro[:last_name] = usu.last_name
                                            registro[:birthday] = usu.birthday
                                            registro[:boss_name] = usu.boss_name
                                            registro[:phone] = usu.phone
                                            registro[:ext] = usu.ext
                                            registro[:profile_id] = usu.profile_id
                                            registro[:area_id] = usu.area_id
                                            registro[:employment] = usu.employment
                                            registro[:activo]= usu.activo

                                            usuarios_arr.push(registro)
                                          end

                                          @subarea_8 = Area.where("area_id = ?", sub7.id)

                                          if !@subarea_8.nil?

                                            @subarea_8.each do |sub8|
                                              valor = {id: '', name: '', area_id: ''}
                                              valor[:id] = sub8.id
                                              valor[:name] = sub8.name
                                              valor[:area_id] = sub8.area_id

                                              areas_arr.push(valor)

                                              @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub8.id)

                                              @usuarios.each do |usu|
                                                registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                                registro[:id] = usu.id
                                                registro[:email] = usu.email
                                                registro[:user_name] = usu.user_name
                                                registro[:name] = usu.name
                                                registro[:last_name] = usu.last_name
                                                registro[:birthday] = usu.birthday
                                                registro[:boss_name] = usu.boss_name
                                                registro[:phone] = usu.phone
                                                registro[:ext] = usu.ext
                                                registro[:profile_id] = usu.profile_id
                                                registro[:area_id] = usu.area_id
                                                registro[:employment] = usu.employment
                                                registro[:activo]= usu.activo

                                                usuarios_arr.push(registro)
                                              end

                                              @subarea_9 = Area.where("area_id = ?", sub8.id)

                                              if !@subarea_9.nil?

                                                @subarea_9.each do |sub9|
                                                  valor = {id: '', name: '', area_id: ''}
                                                  valor[:id] = sub9.id
                                                  valor[:name] = sub9.name
                                                  valor[:area_id] = sub9.area_id

                                                  areas_arr.push(valor)

                                                  @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub9.id)

                                                  @usuarios.each do |usu|
                                                    registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                                    registro[:id] = usu.id
                                                    registro[:email] = usu.email
                                                    registro[:user_name] = usu.user_name
                                                    registro[:name] = usu.name
                                                    registro[:last_name] = usu.last_name
                                                    registro[:birthday] = usu.birthday
                                                    registro[:boss_name] = usu.boss_name
                                                    registro[:phone] = usu.phone
                                                    registro[:ext] = usu.ext
                                                    registro[:profile_id] = usu.profile_id
                                                    registro[:area_id] = usu.area_id
                                                    registro[:employment] = usu.employment
                                                    registro[:activo]= usu.activo

                                                    usuarios_arr.push(registro)
                                                  end

                                                  @subarea_10 = Area.where("area_id = ?", sub9.id)

                                                  if !@subarea_10.nil?

                                                    @subarea_10.each do |sub10|
                                                      valor = {id: '', name: '', area_id: ''}
                                                      valor[:id] = sub10.id
                                                      valor[:name] = sub10.name
                                                      valor[:area_id] = sub10.area_id

                                                      areas_arr.push(valor)

                                                      @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub10.id)

                                                      @usuarios.each do |usu|
                                                        registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                                        registro[:id] = usu.id
                                                        registro[:email] = usu.email
                                                        registro[:user_name] = usu.user_name
                                                        registro[:name] = usu.name
                                                        registro[:last_name] = usu.last_name
                                                        registro[:birthday] = usu.birthday
                                                        registro[:boss_name] = usu.boss_name
                                                        registro[:phone] = usu.phone
                                                        registro[:ext] = usu.ext
                                                        registro[:profile_id] = usu.profile_id
                                                        registro[:area_id] = usu.area_id
                                                        registro[:employment] = usu.employment
                                                        registro[:activo]= usu.activo

                                                        usuarios_arr.push(registro)
                                                      end
                                                    end
                                                  end
                                                end
                                              end
                                            end
                                          end
                                        end
                                      end
                                    end
                                  end
                                end
                              end
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end
            end

            @areas = areas_arr
            @users = Kaminari.paginate_array(usuarios_arr).page(params[:page]).per(10)
          end


          if params[:search].present?
            area = params[:search]
            area = area.gsub! /\t/, ''
            if area.nil?
              @area = params[:search].to_s
            else
              @area = area
            end

            @areas = Area.find_by_name(@area)
            if @areas.present?
              @area_id = @areas.id
            else
              @area_id = "0"
            end

            @perfil = params[:search].to_s
            @perfiles = Profile.find_by_name(@perfil)
            if @perfiles.present?
              @profile_id = @perfiles.id
            else
              @profile_id = "0"
            end

            mailr = params[:search]
            mailr = mailr.gsub! /\t/, ''
            if mailr.nil?
              @mail = params[:search].to_s
            else
              @mail = mailr
            end


            if !User.find_by_name(params[:search]).nil?
              if @cu_area == "1"
                @users = User.where("name = ? and profile_id IS NOT NULL and area_id IS NOT NULL", params[:search].to_s).page(params[:page]).per(10)
              else
                @users = User.where("name = ? and profile_id IS NOT NULL and area_id = ?", params[:search].to_s, @cu_area).page(params[:page]).per(10)
              end
              @query = params[:search]
              @cuenta = @users.length
              if @cuenta == 0
                @error = t('all.error')
                @query = params[:search]
                if @cu_area == "1"
                  @users = User.where("profile_id IS NOT NULL and area_id IS NOT NULL").page(params[:page]).per(10)
                else
                  @users = User.where("profile_id IS NOT NULL and area_id = ?", @cu_area).page(params[:page]).per(10)
                end
              end
            elsif !User.find_by_last_name(params[:search]).nil?
              if @cu_area == "1"
                @users = User.where("last_name = ? and profile_id IS NOT NULL and area_id IS NOT NULL", params[:search].to_s).page(params[:page]).per(10)
              else
                @users = User.where("last_name = ? and profile_id IS NOT NULL and area_id = ?", params[:search].to_s, @cu_area).page(params[:page]).per(10)
              end
              @query = params[:search]
              @cuenta = @users.length

              if @cuenta == 0
                @error = t('all.error')
                @query = params[:search]
                if @cu_area == "1"
                  @users = User.where("profile_id IS NOT NULL and area_id IS NOT NULL").page(params[:page]).per(10)
                else #CUALQUIER OTRA ÁREA, MUESTRA SÓLO LA INFORMACIÓN DEL ÁREA.
                  @users = User.where("profile_id IS NOT NULL and area_id = ?", @cu_area).page(params[:page]).per(10)
                end
              end
            elsif !User.find_by_user_name(params[:search]).nil?
              if @cu_area == "1"
                @users = User.where("user_name = ? and profile_id IS NOT NULL and area_id IS NOT NULL", params[:search].to_s).page(params[:page]).per(10)
              else
                @users = User.where("user_name = ? and profile_id IS NOT NULL and area_id = ?", params[:search].to_s, @cu_area).page(params[:page]).per(10)
              end
              @query = params[:search]
              @cuenta = @users.length

              if @cuenta == 0
                @error = t('all.error')
                @query = params[:search]
                if @cu_area == "1"
                  @users = User.where("profile_id IS NOT NULL and area_id IS NOT NULL").page(params[:page]).per(10)
                else #CUALQUIER OTRA ÁREA, MUESTRA SÓLO LA INFORMACIÓN DEL ÁREA.
                  @users = User.where("profile_id IS NOT NULL and area_id = ?", @cu_area).page(params[:page]).per(10)
                end
              end
            elsif !User.find_by_email(@mail).nil?
              if @cu_area == "1"
                @users = User.where("email = ? and profile_id IS NOT NULL and area_id IS NOT NULL", params[:search].to_s).page(params[:page]).per(10)
              else
                @users = User.where("email = ? and profile_id IS NOT NULL and area_id = ?", params[:search].to_s, @cu_area).page(params[:page]).per(10)
              end
              @query = params[:search]
              @cuenta = @users.length

              if @cuenta == 0
                @error = t('all.error')
                @query = params[:search]
                if @cu_area == "1"
                  @users = User.where("profile_id IS NOT NULL and area_id IS NOT NULL").page(params[:page]).per(10)
                else #CUALQUIER OTRA ÁREA, MUESTRA SÓLO LA INFORMACIÓN DEL ÁREA.
                  @users = User.where("profile_id IS NOT NULL and area_id = ?", @cu_area).page(params[:page]).per(10)
                end
              end
            elsif !User.find_by_profile_id(@profile_id).nil?
              if @cu_area == "1"
                @users = User.where("profile_id = ? and profile_id IS NOT NULL and area_id IS NOT NULL", @profile_id).page(params[:page]).per(10)
              else
                @users = User.where("profile_id = ? and profile_id IS NOT NULL and area_id = ?", @profile_id, @cu_area).page(params[:page]).per(10)
              end
              @query = params[:search]
              @cuenta = @users.length

              if @cuenta == 0
                @error = t('all.error')
                @query = params[:search]
                if @cu_area == "1"
                  @users = User.where("profile_id IS NOT NULL and area_id IS NOT NULL").page(params[:page]).per(10)
                else #CUALQUIER OTRA ÁREA, MUESTRA SÓLO LA INFORMACIÓN DEL ÁREA.
                  @users = User.where("profile_id IS NOT NULL and area_id = ?", @cu_area).page(params[:page]).per(10)
                end
              end
            elsif !User.find_by_area_id(@area_id.to_i).nil?
              if @cu_area == "1"
                @users = User.where("area_id = ? and profile_id IS NOT NULL and area_id IS NOT NULL", @area_id).page(params[:page]).per(10)
              else
                @users = User.where("area_id = ? and profile_id IS NOT NULL and area_id = ?", @area_id, @cu_area).page(params[:page]).per(10)
              end
              @query = params[:search]
              @cuenta = @users.length

              if @cuenta == 0
                @error = t('all.error')
                @query = params[:search]
                if @cu_area == "1"
                  @users = User.where("profile_id IS NOT NULL and area_id IS NOT NULL").page(params[:page]).per(10)
                else #CUALQUIER OTRA ÁREA, MUESTRA SÓLO LA INFORMACIÓN DEL ÁREA.
                  @users = User.where("profile_id IS NOT NULL and area_id = ?", @cu_area).page(params[:page]).per(10)
                end
              end
            elsif !User.find_by_store_id(@belongs).nil?
              if @busco == "2"
                if @cu_area == "1"
                  @users = User.where("store_id IS NULL and profile_id IS NOT NULL and area_id IS NOT NULL").page(params[:page]).per(10)
                else
                  @users = User.where("store_id IS NULL and profile_id IS NOT NULL and area_id = ?", @cu_area).page(params[:page]).per(10)
                end
                @query = params[:search]
                @cuenta = @users.length

                if @cuenta == 0
                  @error = t('all.error')
                  @query = params[:search]
                  if @cu_area == "1"
                    @users = User.where("profile_id IS NOT NULL and area_id IS NOT NULL").page(params[:page]).per(10)
                  else #CUALQUIER OTRA ÁREA, MUESTRA SÓLO LA INFORMACIÓN DEL ÁREA.
                    @users = User.where("profile_id IS NOT NULL and area_id = ?", @cu_area).page(params[:page]).per(10)
                  end
                end
              elsif @busco == "1"
                if @cu_area == "1"
                  @users = User.where("store_id = ? and profile_id IS NOT NULL and area_id IS NOT NULL", @belongs).page(params[:page]).per(10)
                else
                  @users = User.where("store_id = ? and profile_id IS NOT NULL and area_id = ?", @belongs, @cu_area).page(params[:page]).per(10)
                end
                @query = params[:search]
                @cuenta = @users.length

                if @cuenta == 0
                  @error = t('all.error')
                  @query = params[:search]
                  if @cu_area == "1"
                    @users = User.where("profile_id IS NOT NULL and area_id IS NOT NULL").page(params[:page]).per(10)
                  else #CUALQUIER OTRA ÁREA, MUESTRA SÓLO LA INFORMACIÓN DEL ÁREA.
                    @users = User.where("profile_id IS NOT NULL and area_id = ?", @cu_area).page(params[:page]).per(10)
                  end
                end
              else
                @error = t('all.error')
                @query = params[:search]
                if @cu_area == "1"
                  @users = User.where("profile_id IS NOT NULL and area_id IS NOT NULL").page(params[:page]).per(10)
                else #CUALQUIER OTRA ÁREA, MUESTRA SÓLO LA INFORMACIÓN DEL ÁREA.
                  @users = User.where("profile_id IS NOT NULL and area_id = ?", @cu_area).page(params[:page]).per(10)
                end
              end
            else
              @error = t('all.error')
              @query = params[:search]
              if @cu_area == "1"
                @users = User.where("profile_id IS NOT NULL and area_id IS NOT NULL").page(params[:page]).per(10)
              else #CUALQUIER OTRA ÁREA, MUESTRA SÓLO LA INFORMACIÓN DEL ÁREA.
                @users = User.where("profile_id IS NOT NULL and area_id = ?", @cu_area).page(params[:page]).per(10)
              end
            end
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
        #end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  def show
    if current_user
      @page = "Users"
      @user = current_user.name
      @user_id = current_user.id
      @userl = current_user.last_name
      @action = "5"
      @account = "not yet"
      @lasac = "not available"
      @cli_id = 0
      #@det = @loyalty_client.params[:id]
      @date = Time.now

      # @historic = Historic.new
      # @historic.page = @page
      # @historic.user_name = @user + " " + @userl
      # @historic.user_id = @user_id
      # #@historic.action = @det
      # @historic.action = @action
      # @historic.account = @account
      # @historic.last_account = @lasac
      # @historic.client_id = @cli_id
      # @historic.date = @date
      # @historic.save


      @user = User.find(params[:id])
      pd = Profile.all
      a = Area.all

      gon.area = a
      gon.pflag = pd
      gon.false = true
      @cu = current_user.profile_id

      if @cu != 0
        @per = Permission.where("view_name = 'Users' and profile_id = ?", @cu)
        @perpro = Permission.where("view_name = 'Profiles' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarUser = permisos.editar

          if permisos.view_name == @@byUsers

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.view_name
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end
        end

        @perpro.each do |permisos|
          @crearPerfil = permisos.crear

          if permisos.view_name == "Profiles"

            @@crearper = permisos.crear
            @@editarper = permisos.editar
            @@leerper = permisos.leer
            @@eliminarper = permisos.eliminar

            @crearper = permisos.view_name
            @editarper = permisos.editar
            @leerper = permisos.leer
            @eliminarper = permisos.eliminar
          end
        end

        if ((@@leer == 2))
          @area_usuario = current_user.area_id

          if @area_usuario == "1"
            @user = User.find(params[:id])

          else
            @areas = Area.where("id = ?", @area_usuario)

            areas_arr = Array.new
            usuarios_arr = Array.new

            @areas.each do |ar|
              @nombre = ar.name
              valor = {id: '', name: '', area_id: ''}
              valor[:id] = ar.id
              valor[:name] = ar.name
              valor[:area_id] = ar.area_id

              areas_arr.push(valor)

              @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", ar.id)

              @usuarios.each do |usu|
                registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                registro[:id] = usu.id
                registro[:email] = usu.email
                registro[:user_name] = usu.user_name
                registro[:name] = usu.name
                registro[:last_name] = usu.last_name
                registro[:birthday] = usu.birthday
                registro[:boss_name] = usu.boss_name
                registro[:phone] = usu.phone
                registro[:ext] = usu.ext
                registro[:profile_id] = usu.profile_id
                registro[:area_id] = usu.area_id
                registro[:employment] = usu.employment
                registro[:activo]= usu.activo

                usuarios_arr.push(registro)
              end

              @subarea_1 = Area.where("area_id = ?", ar.id)

              if !@subarea_1.nil?

                @subarea_1.each do |sub|
                  valor = {id: '', name: '', area_id: ''}
                  valor[:id] = sub.id
                  valor[:name] = sub.name
                  valor[:area_id] = sub.area_id

                  areas_arr.push(valor)

                  @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub.id)

                  @usuarios.each do |usu|
                    registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                    registro[:id] = usu.id
                    registro[:email] = usu.email
                    registro[:user_name] = usu.user_name
                    registro[:name] = usu.name
                    registro[:last_name] = usu.last_name
                    registro[:birthday] = usu.birthday
                    registro[:boss_name] = usu.boss_name
                    registro[:phone] = usu.phone
                    registro[:ext] = usu.ext
                    registro[:profile_id] = usu.profile_id
                    registro[:area_id] = usu.area_id
                    registro[:employment] = usu.employment
                    registro[:activo]= usu.activo

                    usuarios_arr.push(registro)
                  end

                  @subarea_2 = Area.where("area_id = ?", sub.id)

                  if !@subarea_2.nil?

                    @subarea_2.each do |sub2|
                      valor = {id: '', name: '', area_id: ''}
                      valor[:id] = sub2.id
                      valor[:name] = sub2.name
                      valor[:area_id] = sub2.area_id

                      areas_arr.push(valor)

                      @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub2.id)

                      @usuarios.each do |usu|
                        registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                        registro[:id] = usu.id
                        registro[:email] = usu.email
                        registro[:user_name] = usu.user_name
                        registro[:name] = usu.name
                        registro[:last_name] = usu.last_name
                        registro[:birthday] = usu.birthday
                        registro[:boss_name] = usu.boss_name
                        registro[:phone] = usu.phone
                        registro[:ext] = usu.ext
                        registro[:profile_id] = usu.profile_id
                        registro[:area_id] = usu.area_id
                        registro[:employment] = usu.employment
                        registro[:activo]= usu.activo

                        usuarios_arr.push(registro)
                      end

                      @subarea_3 = Area.where("area_id = ?", sub2.id)

                      if !@subarea_3.nil?

                        @subarea_3.each do |sub3|
                          valor = {id: '', name: '', area_id: ''}
                          valor[:id] = sub3.id
                          valor[:name] = sub3.name
                          valor[:area_id] = sub3.area_id

                          areas_arr.push(valor)

                          @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub3.id)

                          @usuarios.each do |usu|
                            registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                            registro[:id] = usu.id
                            registro[:email] = usu.email
                            registro[:user_name] = usu.user_name
                            registro[:name] = usu.name
                            registro[:last_name] = usu.last_name
                            registro[:birthday] = usu.birthday
                            registro[:boss_name] = usu.boss_name
                            registro[:phone] = usu.phone
                            registro[:ext] = usu.ext
                            registro[:profile_id] = usu.profile_id
                            registro[:area_id] = usu.area_id
                            registro[:employment] = usu.employment
                            registro[:activo]= usu.activo

                            usuarios_arr.push(registro)
                          end

                          @subarea_4 = Area.where("area_id = ?", sub3.id)

                          if !@subarea_4.nil?

                            @subarea_4.each do |sub4|
                              valor = {id: '', name: '', area_id: ''}
                              valor[:id] = sub4.id
                              valor[:name] = sub4.name
                              valor[:area_id] = sub4.area_id

                              areas_arr.push(valor)

                              @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub4.id)

                              @usuarios.each do |usu|
                                registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                registro[:id] = usu.id
                                registro[:email] = usu.email
                                registro[:user_name] = usu.user_name
                                registro[:name] = usu.name
                                registro[:last_name] = usu.last_name
                                registro[:birthday] = usu.birthday
                                registro[:boss_name] = usu.boss_name
                                registro[:phone] = usu.phone
                                registro[:ext] = usu.ext
                                registro[:profile_id] = usu.profile_id
                                registro[:area_id] = usu.area_id
                                registro[:employment] = usu.employment
                                registro[:activo]= usu.activo

                                usuarios_arr.push(registro)
                              end

                              @subarea_5 = Area.where("area_id = ?", sub4.id)

                              if !@subarea_5.nil?

                                @subarea_5.each do |sub5|
                                  valor = {id: '', name: '', area_id: ''}
                                  valor[:id] = sub5.id
                                  valor[:name] = sub5.name
                                  valor[:area_id] = sub5.area_id

                                  areas_arr.push(valor)

                                  @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub5.id)

                                  @usuarios.each do |usu|
                                    registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                    registro[:id] = usu.id
                                    registro[:email] = usu.email
                                    registro[:user_name] = usu.user_name
                                    registro[:name] = usu.name
                                    registro[:last_name] = usu.last_name
                                    registro[:birthday] = usu.birthday
                                    registro[:boss_name] = usu.boss_name
                                    registro[:phone] = usu.phone
                                    registro[:ext] = usu.ext
                                    registro[:profile_id] = usu.profile_id
                                    registro[:area_id] = usu.area_id
                                    registro[:employment] = usu.employment
                                    registro[:activo]= usu.activo

                                    usuarios_arr.push(registro)
                                  end

                                  @subarea_6 = Area.where("area_id = ?", sub5.id)

                                  if !@subarea_6.nil?

                                    @subarea_6.each do |sub6|
                                      valor = {id: '', name: '', area_id: ''}
                                      valor[:id] = sub6.id
                                      valor[:name] = sub6.name
                                      valor[:area_id] = sub6.area_id

                                      areas_arr.push(valor)

                                      @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub6.id)

                                      @usuarios.each do |usu|
                                        registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                        registro[:id] = usu.id
                                        registro[:email] = usu.email
                                        registro[:user_name] = usu.user_name
                                        registro[:name] = usu.name
                                        registro[:last_name] = usu.last_name
                                        registro[:birthday] = usu.birthday
                                        registro[:boss_name] = usu.boss_name
                                        registro[:phone] = usu.phone
                                        registro[:ext] = usu.ext
                                        registro[:profile_id] = usu.profile_id
                                        registro[:area_id] = usu.area_id
                                        registro[:employment] = usu.employment
                                        registro[:activo]= usu.activo

                                        usuarios_arr.push(registro)
                                      end

                                      @subarea_7 = Area.where("area_id = ?", sub6.id)

                                      if !@subarea_7.nil?

                                        @subarea_7.each do |sub7|
                                          valor = {id: '', name: '', area_id: ''}
                                          valor[:id] = sub7.id
                                          valor[:name] = sub7.name
                                          valor[:area_id] = sub7.area_id

                                          areas_arr.push(valor)

                                          @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub7.id)

                                          @usuarios.each do |usu|
                                            registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                            registro[:id] = usu.id
                                            registro[:email] = usu.email
                                            registro[:user_name] = usu.user_name
                                            registro[:name] = usu.name
                                            registro[:last_name] = usu.last_name
                                            registro[:birthday] = usu.birthday
                                            registro[:boss_name] = usu.boss_name
                                            registro[:phone] = usu.phone
                                            registro[:ext] = usu.ext
                                            registro[:profile_id] = usu.profile_id
                                            registro[:area_id] = usu.area_id
                                            registro[:employment] = usu.employment
                                            registro[:activo]= usu.activo

                                            usuarios_arr.push(registro)
                                          end

                                          @subarea_8 = Area.where("area_id = ?", sub7.id)

                                          if !@subarea_8.nil?

                                            @subarea_8.each do |sub8|
                                              valor = {id: '', name: '', area_id: ''}
                                              valor[:id] = sub8.id
                                              valor[:name] = sub8.name
                                              valor[:area_id] = sub8.area_id

                                              areas_arr.push(valor)

                                              @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub8.id)

                                              @usuarios.each do |usu|
                                                registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                                registro[:id] = usu.id
                                                registro[:email] = usu.email
                                                registro[:user_name] = usu.user_name
                                                registro[:name] = usu.name
                                                registro[:last_name] = usu.last_name
                                                registro[:birthday] = usu.birthday
                                                registro[:boss_name] = usu.boss_name
                                                registro[:phone] = usu.phone
                                                registro[:ext] = usu.ext
                                                registro[:profile_id] = usu.profile_id
                                                registro[:area_id] = usu.area_id
                                                registro[:employment] = usu.employment
                                                registro[:activo]= usu.activo

                                                usuarios_arr.push(registro)
                                              end

                                              @subarea_9 = Area.where("area_id = ?", sub8.id)

                                              if !@subarea_9.nil?

                                                @subarea_9.each do |sub9|
                                                  valor = {id: '', name: '', area_id: ''}
                                                  valor[:id] = sub9.id
                                                  valor[:name] = sub9.name
                                                  valor[:area_id] = sub9.area_id

                                                  areas_arr.push(valor)

                                                  @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub9.id)

                                                  @usuarios.each do |usu|
                                                    registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                                    registro[:id] = usu.id
                                                    registro[:email] = usu.email
                                                    registro[:user_name] = usu.user_name
                                                    registro[:name] = usu.name
                                                    registro[:last_name] = usu.last_name
                                                    registro[:birthday] = usu.birthday
                                                    registro[:boss_name] = usu.boss_name
                                                    registro[:phone] = usu.phone
                                                    registro[:ext] = usu.ext
                                                    registro[:profile_id] = usu.profile_id
                                                    registro[:area_id] = usu.area_id
                                                    registro[:employment] = usu.employment
                                                    registro[:activo]= usu.activo

                                                    usuarios_arr.push(registro)
                                                  end

                                                  @subarea_10 = Area.where("area_id = ?", sub9.id)

                                                  if !@subarea_10.nil?

                                                    @subarea_10.each do |sub10|
                                                      valor = {id: '', name: '', area_id: ''}
                                                      valor[:id] = sub10.id
                                                      valor[:name] = sub10.name
                                                      valor[:area_id] = sub10.area_id

                                                      areas_arr.push(valor)

                                                      @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub10.id)

                                                      @usuarios.each do |usu|
                                                        registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                                        registro[:id] = usu.id
                                                        registro[:email] = usu.email
                                                        registro[:user_name] = usu.user_name
                                                        registro[:name] = usu.name
                                                        registro[:last_name] = usu.last_name
                                                        registro[:birthday] = usu.birthday
                                                        registro[:boss_name] = usu.boss_name
                                                        registro[:phone] = usu.phone
                                                        registro[:ext] = usu.ext
                                                        registro[:profile_id] = usu.profile_id
                                                        registro[:area_id] = usu.area_id
                                                        registro[:employment] = usu.employment
                                                        registro[:activo]= usu.activo

                                                        usuarios_arr.push(registro)
                                                      end
                                                    end
                                                  end
                                                end
                                              end
                                            end
                                          end
                                        end
                                      end
                                    end
                                  end
                                end
                              end
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end
            end

            @usuario_id = params[:id]
            @visible = false

            usuarios_arr.each do |usu_visible|
              @id_actual = usu_visible[:id]
              if @id_actual == @usuario_id.to_i
                @visible = true
                break
              else
                @visible = false
              end
            end

            if @visible == true
              @user = User.find(@usuario_id)
            else
              @Without_Permission = 100
              redirect_to home_index_path, :alert => t('all.not_access')
            end

          end

        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  def edit
    if current_user
      @userEdit = User.find(params[:id])
      pd = Profile.all
      a = Area.all

      gon.area = a
      gon.pflag = pd
      gon.false = true
      @cu = current_user.profile_id

      if @cu != 0

        @per = Permission.where("view_name = 'Users' and profile_id = ?", @cu)
        @perpro = Permission.where("view_name = 'Profiles' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarUser = permisos.editar
          @leerUser = permisos.leer
          @fla = permisos.profile.flag
          @ar = permisos.profile.area.id

          if permisos.view_name == @@byUsers

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.view_name
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end
        end

        @perpro.each do |permisos|
          @crearPerfil = permisos.crear

          if permisos.view_name == "Profiles"

            @@crearper = permisos.crear
            @@editarper = permisos.editar
            @@leerper = permisos.leer
            @@eliminarper = permisos.eliminar

            @crearper = permisos.view_name
            @editarper = permisos.editar
            @leerper = permisos.leer
            @eliminarper = permisos.eliminar
          end
        end

        @area_usuario = current_user.area_id

        if @area_usuario == "1"
          @areas = Area.all

          areas_arr = Array.new

          @areas.each do |ar|
            valor = {id: '', name: '', area_id: ''}
            valor[:id] = ar.id
            valor[:name] = ar.name
            valor[:area_id] = ar.area_id

            areas_arr.push(valor)
          end

          @areas = areas_arr
        else
          @areas = Area.where("id = ?", @area_usuario)

          areas_arr = Array.new
          usuarios_arr = Array.new

          @areas.each do |ar|
            valor = {id: '', name: '', area_id: ''}
            valor[:id] = ar.id
            valor[:name] = ar.name
            valor[:area_id] = ar.area_id

            areas_arr.push(valor)

            @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", ar.id)

            @usuarios.each do |usu|
              registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
              registro[:id] = usu.id
              registro[:email] = usu.email
              registro[:user_name] = usu.user_name
              registro[:name] = usu.name
              registro[:last_name] = usu.last_name
              registro[:birthday] = usu.birthday
              registro[:boss_name] = usu.boss_name
              registro[:phone] = usu.phone
              registro[:ext] = usu.ext
              registro[:profile_id] = usu.profile_id
              registro[:area_id] = usu.area_id
              registro[:employment] = usu.employment
              registro[:activo]= usu.activo

              usuarios_arr.push(registro)
            end

            @subarea_1 = Area.where("area_id = ?", ar.id)

            if !@subarea_1.nil?

              @subarea_1.each do |sub|
                valor = {id: '', name: '', area_id: ''}
                valor[:id] = sub.id
                valor[:name] = sub.name
                valor[:area_id] = sub.area_id

                areas_arr.push(valor)

                @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub.id)

                @usuarios.each do |usu|
                  registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                  registro[:id] = usu.id
                  registro[:email] = usu.email
                  registro[:user_name] = usu.user_name
                  registro[:name] = usu.name
                  registro[:last_name] = usu.last_name
                  registro[:birthday] = usu.birthday
                  registro[:boss_name] = usu.boss_name
                  registro[:phone] = usu.phone
                  registro[:ext] = usu.ext
                  registro[:profile_id] = usu.profile_id
                  registro[:area_id] = usu.area_id
                  registro[:employment] = usu.employment
                  registro[:activo]= usu.activo

                  usuarios_arr.push(registro)
                end

                @subarea_2 = Area.where("area_id = ?", sub.id)

                if !@subarea_2.nil?

                  @subarea_2.each do |sub2|
                    valor = {id: '', name: '', area_id: ''}
                    valor[:id] = sub2.id
                    valor[:name] = sub2.name
                    valor[:area_id] = sub2.area_id

                    areas_arr.push(valor)

                    @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub2.id)

                    @usuarios.each do |usu|
                      registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                      registro[:id] = usu.id
                      registro[:email] = usu.email
                      registro[:user_name] = usu.user_name
                      registro[:name] = usu.name
                      registro[:last_name] = usu.last_name
                      registro[:birthday] = usu.birthday
                      registro[:boss_name] = usu.boss_name
                      registro[:phone] = usu.phone
                      registro[:ext] = usu.ext
                      registro[:profile_id] = usu.profile_id
                      registro[:area_id] = usu.area_id
                      registro[:employment] = usu.employment
                      registro[:activo]= usu.activo

                      usuarios_arr.push(registro)
                    end

                    @subarea_3 = Area.where("area_id = ?", sub2.id)

                    if !@subarea_3.nil?

                      @subarea_3.each do |sub3|
                        valor = {id: '', name: '', area_id: ''}
                        valor[:id] = sub3.id
                        valor[:name] = sub3.name
                        valor[:area_id] = sub3.area_id

                        areas_arr.push(valor)

                        @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub3.id)

                        @usuarios.each do |usu|
                          registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                          registro[:id] = usu.id
                          registro[:email] = usu.email
                          registro[:user_name] = usu.user_name
                          registro[:name] = usu.name
                          registro[:last_name] = usu.last_name
                          registro[:birthday] = usu.birthday
                          registro[:boss_name] = usu.boss_name
                          registro[:phone] = usu.phone
                          registro[:ext] = usu.ext
                          registro[:profile_id] = usu.profile_id
                          registro[:area_id] = usu.area_id
                          registro[:employment] = usu.employment
                          registro[:activo]= usu.activo

                          usuarios_arr.push(registro)
                        end

                        @subarea_4 = Area.where("area_id = ?", sub3.id)

                        if !@subarea_4.nil?

                          @subarea_4.each do |sub4|
                            valor = {id: '', name: '', area_id: ''}
                            valor[:id] = sub4.id
                            valor[:name] = sub4.name
                            valor[:area_id] = sub4.area_id

                            areas_arr.push(valor)

                            @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub4.id)

                            @usuarios.each do |usu|
                              registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                              registro[:id] = usu.id
                              registro[:email] = usu.email
                              registro[:user_name] = usu.user_name
                              registro[:name] = usu.name
                              registro[:last_name] = usu.last_name
                              registro[:birthday] = usu.birthday
                              registro[:boss_name] = usu.boss_name
                              registro[:phone] = usu.phone
                              registro[:ext] = usu.ext
                              registro[:profile_id] = usu.profile_id
                              registro[:area_id] = usu.area_id
                              registro[:employment] = usu.employment
                              registro[:activo]= usu.activo

                              usuarios_arr.push(registro)
                            end

                            @subarea_5 = Area.where("area_id = ?", sub4.id)

                            if !@subarea_5.nil?

                              @subarea_5.each do |sub5|
                                valor = {id: '', name: '', area_id: ''}
                                valor[:id] = sub5.id
                                valor[:name] = sub5.name
                                valor[:area_id] = sub5.area_id

                                areas_arr.push(valor)

                                @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub5.id)

                                @usuarios.each do |usu|
                                  registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                  registro[:id] = usu.id
                                  registro[:email] = usu.email
                                  registro[:user_name] = usu.user_name
                                  registro[:name] = usu.name
                                  registro[:last_name] = usu.last_name
                                  registro[:birthday] = usu.birthday
                                  registro[:boss_name] = usu.boss_name
                                  registro[:phone] = usu.phone
                                  registro[:ext] = usu.ext
                                  registro[:profile_id] = usu.profile_id
                                  registro[:area_id] = usu.area_id
                                  registro[:employment] = usu.employment
                                  registro[:activo]= usu.activo

                                  usuarios_arr.push(registro)
                                end

                                @subarea_6 = Area.where("area_id = ?", sub5.id)

                                if !@subarea_6.nil?

                                  @subarea_6.each do |sub6|
                                    valor = {id: '', name: '', area_id: ''}
                                    valor[:id] = sub6.id
                                    valor[:name] = sub6.name
                                    valor[:area_id] = sub6.area_id

                                    areas_arr.push(valor)

                                    @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub6.id)

                                    @usuarios.each do |usu|
                                      registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                      registro[:id] = usu.id
                                      registro[:email] = usu.email
                                      registro[:user_name] = usu.user_name
                                      registro[:name] = usu.name
                                      registro[:last_name] = usu.last_name
                                      registro[:birthday] = usu.birthday
                                      registro[:boss_name] = usu.boss_name
                                      registro[:phone] = usu.phone
                                      registro[:ext] = usu.ext
                                      registro[:profile_id] = usu.profile_id
                                      registro[:area_id] = usu.area_id
                                      registro[:employment] = usu.employment
                                      registro[:activo]= usu.activo

                                      usuarios_arr.push(registro)
                                    end

                                    @subarea_7 = Area.where("area_id = ?", sub6.id)

                                    if !@subarea_7.nil?

                                      @subarea_7.each do |sub7|
                                        valor = {id: '', name: '', area_id: ''}
                                        valor[:id] = sub7.id
                                        valor[:name] = sub7.name
                                        valor[:area_id] = sub7.area_id

                                        areas_arr.push(valor)

                                        @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub7.id)

                                        @usuarios.each do |usu|
                                          registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                          registro[:id] = usu.id
                                          registro[:email] = usu.email
                                          registro[:user_name] = usu.user_name
                                          registro[:name] = usu.name
                                          registro[:last_name] = usu.last_name
                                          registro[:birthday] = usu.birthday
                                          registro[:boss_name] = usu.boss_name
                                          registro[:phone] = usu.phone
                                          registro[:ext] = usu.ext
                                          registro[:profile_id] = usu.profile_id
                                          registro[:area_id] = usu.area_id
                                          registro[:employment] = usu.employment
                                          registro[:activo]= usu.activo

                                          usuarios_arr.push(registro)
                                        end

                                        @subarea_8 = Area.where("area_id = ?", sub7.id)

                                        if !@subarea_8.nil?

                                          @subarea_8.each do |sub8|
                                            valor = {id: '', name: '', area_id: ''}
                                            valor[:id] = sub8.id
                                            valor[:name] = sub8.name
                                            valor[:area_id] = sub8.area_id

                                            areas_arr.push(valor)

                                            @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub8.id)

                                            @usuarios.each do |usu|
                                              registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                              registro[:id] = usu.id
                                              registro[:email] = usu.email
                                              registro[:user_name] = usu.user_name
                                              registro[:name] = usu.name
                                              registro[:last_name] = usu.last_name
                                              registro[:birthday] = usu.birthday
                                              registro[:boss_name] = usu.boss_name
                                              registro[:phone] = usu.phone
                                              registro[:ext] = usu.ext
                                              registro[:profile_id] = usu.profile_id
                                              registro[:area_id] = usu.area_id
                                              registro[:employment] = usu.employment
                                              registro[:activo]= usu.activo

                                              usuarios_arr.push(registro)
                                            end

                                            @subarea_9 = Area.where("area_id = ?", sub8.id)

                                            if !@subarea_9.nil?

                                              @subarea_9.each do |sub9|
                                                valor = {id: '', name: '', area_id: ''}
                                                valor[:id] = sub9.id
                                                valor[:name] = sub9.name
                                                valor[:area_id] = sub9.area_id

                                                areas_arr.push(valor)

                                                @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub9.id)

                                                @usuarios.each do |usu|
                                                  registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                                  registro[:id] = usu.id
                                                  registro[:email] = usu.email
                                                  registro[:user_name] = usu.user_name
                                                  registro[:name] = usu.name
                                                  registro[:last_name] = usu.last_name
                                                  registro[:birthday] = usu.birthday
                                                  registro[:boss_name] = usu.boss_name
                                                  registro[:phone] = usu.phone
                                                  registro[:ext] = usu.ext
                                                  registro[:profile_id] = usu.profile_id
                                                  registro[:area_id] = usu.area_id
                                                  registro[:employment] = usu.employment
                                                  registro[:activo]= usu.activo

                                                  usuarios_arr.push(registro)
                                                end

                                                @subarea_10 = Area.where("area_id = ?", sub9.id)

                                                if !@subarea_10.nil?

                                                  @subarea_10.each do |sub10|
                                                    valor = {id: '', name: '', area_id: ''}
                                                    valor[:id] = sub10.id
                                                    valor[:name] = sub10.name
                                                    valor[:area_id] = sub10.area_id

                                                    areas_arr.push(valor)

                                                    @usuarios = User.where("profile_id IS NOT NULL and area_id = ?", sub10.id)

                                                    @usuarios.each do |usu|
                                                      registro = {id: '', email: '', user_name: '', name: '', last_name: '', birthday: '', boss_name: '', phone: '', ext: '', profile_id: '', area_id: '', employment: '', activo: ''}
                                                      registro[:id] = usu.id
                                                      registro[:email] = usu.email
                                                      registro[:user_name] = usu.user_name
                                                      registro[:name] = usu.name
                                                      registro[:last_name] = usu.last_name
                                                      registro[:birthday] = usu.birthday
                                                      registro[:boss_name] = usu.boss_name
                                                      registro[:phone] = usu.phone
                                                      registro[:ext] = usu.ext
                                                      registro[:profile_id] = usu.profile_id
                                                      registro[:area_id] = usu.area_id
                                                      registro[:employment] = usu.employment
                                                      registro[:activo]= usu.activo

                                                      usuarios_arr.push(registro)
                                                    end
                                                  end
                                                end
                                              end
                                            end
                                          end
                                        end
                                      end
                                    end
                                  end
                                end
                              end
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end
            end
          end

          @areas = areas_arr
          @usuario_id = params[:id]
          @visible = false

          usuarios_arr.each do |usu_visible|
            @id_actual = usu_visible[:id]
            if @id_actual == @usuario_id.to_i
              @visible = true
              break
            else
              @visible = false
            end
          end

          if @visible == true
            if @editarUser == 4 && (@userEdit.id != current_user.id.to_i)
              if current_user.area_id == "1" #Es un usuario de CENTRAL
                if (@userEdit.profile.flag > current_user.profile.flag.to_i)
                  @user = User.find(params[:id])
                else
                  @Without_Permission = 100
                  redirect_to home_index_path, :alert => t('all.not_access')
                end
              else #Es un usuario de OTRA AREA
                if @userEdit.profile.flag > current_user.profile.flag.to_i && @userEdit.area_id.to_i == current_user.area_id.to_i
                  @user = User.find(params[:id])
                end
                if @userEdit.profile.flag >= current_user.profile.flag.to_i && @userEdit.area_id.to_i > current_user.area_id.to_i
                  @user = User.find(params[:id])
                else
                  @Without_Permission = 100
                  redirect_to home_index_path, :alert => t('all.not_access')
                end
              end
            else
              @Without_Permission = 100
              redirect_to home_index_path, :alert => t('all.not_access')
            end
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  def update
    if current_user
      @page = "Users"
      @user = current_user.name
      @user_id = current_user.id
      @userl = current_user.last_name
      @action = "2"
      @account = "not yet"
      @lasac = "not available"
      @cli_id = 0
      #@det = @loyalty_client.params[:id]
      @date = Time.now

      # @historic = Historic.new
      # @historic.page = @page
      # @historic.user_name = @user + " " + @userl
      # @historic.user_id = @user_id
      # #@historic.action = @det
      # @historic.action = @action
      # @historic.account = @account
      # @historic.last_account = @lasac
      # @historic.client_id = @cli_id
      # @historic.date = @date
      # @historic.save

      @userEdit = User.find(params[:id])
      valorPasadoActividad = params[:valorPasado]


      if (@userEdit.last_activity_at.nil?) && (current_user.profile.flag == 0 || current_user.id != @userEdit.id)

        if params[:user][:profile_id].present? && params[:user][:area_id].present?
          if @userEdit.update_attributes(secure_params).present?
            redirect_to users_path, :notice => t('activerecord.atributes.act_pro')
            @userEdit.activo = 0
            @userEdit.last_activity_at = Time.now
            @userEdit.expired_at = nil
            @userEdit.save
            Expired.cuenta_activada(@userEdit).deliver
            ChangePerfil.permisos_otorgados(@userEdit).deliver
          else
            redirect_to users_path, :alert => t('activerecord.atributes.unable')
          end
        else
          @userEdit.name = params[:user][:name]
          @userEdit.last_name = params[:user][:last_name]
          @userEdit.phone = params[:user][:phone]
          @userEdit.ext = params[:user][:ext]
          @userEdit.boss_name = params[:user][:boss_name]
          @userEdit.employment = params[:user][:employment]
          @userEdit.save
          redirect_to users_path, :notice => t('activerecord.atributes.you')
        end

      elsif (@userEdit.last_activity_at < 1.month.ago) && (current_user.profile.flag == 0 || current_user.id != @userEdit.id)
        @userEdit.last_activity_at = Time.now
        @userEdit.expired_at = nil
        @userEdit.save
        Expired.cuenta_activada(@userEdit).deliver
        redirect_to users_path, :notice => t('activerecord.atributes.activated')

      elsif (@userEdit.last_activity_at == valorPasadoActividad) && (current_user.profile.flag != 0 || current_user.id == @userEdit.id)
        @userEdit.last_activity_at = valorPasadoActividad
        @userEdit.save
        redirect_to users_path, :alert => t('activerecord.atributes.not_activated')

      else
        if params[:user][:profile_id].present? && params[:user][:area_id].present?
          # Aquí entra cuando se cambia el perfil
          if @userEdit.update_attributes(secure_params).present?
            redirect_to users_path, :notice => t('activerecord.atributes.user_up')
            @userEdit.activo = 0
            @userEdit.profile_id = params[:user][:profile_id]
            @userEdit.area_id = params[:user][:area_id]
            @userEdit.save
            # @historic.detail = @userEdit.to_json
            # @historic.save
            ChangePerfil.permisos_otorgados(@userEdit).deliver
          else
            redirect_to users_path, :alert => t('activerecord.atributes.unable')
          end
        else
          # Entra aquí cuando se edita el nombre solamente
          @userEdit.name = params[:user][:name]
          @userEdit.last_name = params[:user][:last_name]
          @userEdit.phone = params[:user][:phone]
          @userEdit.ext = params[:user][:ext]
          @userEdit.boss_name = params[:user][:boss_name]
          @userEdit.employment = params[:user][:employment]
          @userEdit.save
          # @historic.detail = @userEdit.to_json
          # @historic.save
          redirect_to users_path, :notice => t('activerecord.atributes.you')
        end
      end
    else
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  def permissions
    @user = current_user
    @admin = User.where("profile_id = 1")
    @admin.each do |f|
      @email = f.email
      ChangePerfil.mandar_permisos(@user, @email).deliver
    end
    redirect_to home_index_path, :notice => t('activerecord.atributes.home')
  end

  def destroy
    if current_user
      @page = "Users"
      @user = current_user.name
      @user_id = current_user.id
      @userl = current_user.last_name
      @action = "4"
      @account = "not yet"
      @lasac = "not available"
      @cli_id = 0
      #@det = @loyalty_client.params[:id]
      @date = Time.now

      # @historic = Historic.new
      # @historic.page = @page
      # @historic.user_name = @user + " " + @userl
      # @historic.user_id = @user_id
      # #@historic.action = @det
      # @historic.action = @action
      # @historic.account = @account
      # @historic.last_account = @lasac
      # @historic.client_id = @cli_id
      # @historic.date = @date
      # @historic.save


      @cu = current_user.profile_id

      if @cu != 0

        @per = Permission.where("view_name = 'Users' and profile_id = ?", @cu)

        @per.each do |permisos|
          @eliminarUser = permisos.eliminar
          @fla = permisos.profile.flag
          @ar = permisos.profile.area_id

          if permisos.view_name == @@byUsers

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar
          end

        end
        @fla = current_user.profile.flag
        @ar = current_user.area_id
        @userDelete = User.find(params[:id])

        if @eliminarUser == 1 && (@userDelete.id != current_user.id.to_i)
          if current_user.area_id == "1" #Es un usuario de CENTRAL
            if (@userDelete.profile.flag > current_user.profile.flag.to_i)
              user = User.find(params[:id])
              user.destroy
              # @historic.detail = user.to_json
              # @historic.save
              redirect_to users_path, :notice => t('views.mess_cont_us_del')
            end
          else #Es un usuario de OTRA AREA
            if @userDelete.profile.flag > current_user.profile.flag && @userDelete.area_id.to_i == current_user.area_id.to_i
              user = User.find(params[:id])
              user.destroy
              # @historic.detail = user.to_json
              # @historic.save
              redirect_to users_path, :notice => t('views.mess_cont_us_del')
            end
            if @userDelete.profile.flag >= current_user.profile.flag && @userDelete.area_id.to_i > current_user.area_id.to_i
              user = User.find(params[:id])
              user.destroy
              # @historic.detail = user.to_json
              # @historic.save
              redirect_to users_path, :notice => t('views.mess_cont_us_del')
            else
              @Without_Permission = 100
              redirect_to home_index_path, :alert => t('all.not_access')
            end
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :encrypted_password, :reset_password_token, :reset_password_sent_at, :remember_created_at, :sign_in_count, :current_sing_in_at, :last_sign_in_at, :confirmation_token, :confirmed_at, :confirmation_sent_at, :unconfirmed_email, :failed_attempts, :unlock_token, :locked_at, :created_at, :updated_at, :username, :name, :password, :password_confirmation, :last_name, :birthday, :boss_name, :phone, :ext, :profile_id, :employment, :role, :area_id, :activo, :last_activity_at, :expired_at)
  end

  def secure_params
    params.require(:user).permit(:profile_id, :area_id, :name, :last_name, :phone, :boss_name, :employment)
  end

end