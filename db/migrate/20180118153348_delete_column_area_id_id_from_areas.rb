class DeleteColumnAreaIdIdFromAreas < ActiveRecord::Migration[5.0]
  def change
    remove_column :areas, :area_id_id
  end
end
